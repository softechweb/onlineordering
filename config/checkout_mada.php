<?php

return [
    [
        'Bank_Name'=>'Al Bank Al Saudi Al Fransi',
        'bin'=>588845,
        'PAN_Length'=>'18',
        'Card_Brand'=>'mada'
    ],



    [
        'Bank_Name'=>'Al Bank Al Saudi Al Fransi',
        'bin'=>440647,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],

    [
        'Bank_Name'=>'Al Bank Al Saudi Al Fransi',
        'bin'=>440795,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Al Bank Al Saudi Al Fransi',
        'bin'=>446404,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Al Bank Al Saudi Al Fransi',
        'bin'=>457865,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Al Bank Al Saudi Al Fransi',
        'bin'=>968208,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada'
    ],



    [
        'Bank_Name'=>'Al Bank Al Awwal',
        'bin'=>588846,
        'PAN_Length'=>'18',
        'Card_Brand'=>'mada'
    ],



    [
        'Bank_Name'=>'Al Bank Al Awwal',
        'bin'=>493428,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Al Bank Al Awwal',
        'bin'=>539931,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'Al Bank Al Awwal',
        'bin'=>558848,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'Al Bank Al Awwal',
        'bin'=>557606,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'Al Bank Al Awwal',
        'bin'=>968210,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada'
    ],



    [
        'Bank_Name'=>'Al Bilad Bank',
        'bin'=>636120,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada'
    ],



    [
        'Bank_Name'=>'Al Bilad Bank',
        'bin'=>417633,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Al Bilad Bank',
        'bin'=>468540,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Al Bilad Bank',
        'bin'=>468541,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Al Bilad Bank',
        'bin'=>468542,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Al Bilad Bank',
        'bin'=>468543,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Al Bilad Bank',
        'bin'=>968201,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada'
    ],



    [
        'Bank_Name'=>'Al Bilad Bank',
        'bin'=>446393,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Al Rajhi Banking & Inv. Corp.',
        'bin'=>588847,
        'PAN_Length'=>'19',
        'Card_Brand'=>'mada'
    ],



    [
        'Bank_Name'=>'Al Rajhi Banking & Inv. Corp.',
        'bin'=>400861,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Al Rajhi Banking & Inv. Corp.',
        'bin'=>409201,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Al Rajhi Banking & Inv. Corp.',
        'bin'=>458456,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Al Rajhi Banking & Inv. Corp.',
        'bin'=>484783,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Al Rajhi Banking & Inv. Corp.',
        'bin'=>968205,
        'PAN_Length'=>'19',
        'Card_Brand'=>'mada'
    ],



    [
        'Bank_Name'=>'Al Rajhi Banking & Inv. Corp.',
        'bin'=>462220,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Al Rajhi Banking & Inv. Corp.',
        'bin'=>455708,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Arab National Bank',
        'bin'=>588848,
        'PAN_Length'=>'19',
        'Card_Brand'=>'mada'
    ],



    [
        'Bank_Name'=>'Arab National Bank',
        'bin'=>455036,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Arab National Bank',
        'bin'=>968203,
        'PAN_Length'=>'19',
        'Card_Brand'=>'mada'
    ],



    [
        'Bank_Name'=>'Arab National Bank',
        'bin'=>486094,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Arab National Bank',
        'bin'=>486095,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Arab National Bank',
        'bin'=>486096,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Bank Al-Jazira',
        'bin'=>504300,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada'
    ],



    [
        'Bank_Name'=>'Bank Al-Jazira',
        'bin'=>440533,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Bank Al-Jazira',
        'bin'=>489317,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Bank Al-Jazira',
        'bin'=>489318,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Bank Al-Jazira',
        'bin'=>489319,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Bank Al-Jazira',
        'bin'=>445564,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Bank Al-Jazira',
        'bin'=>968211,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada'
    ],



    [
        'Bank_Name'=>'Bank Al-Jazira',
        'bin'=>401757,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada'
    ],



    [
        'Bank_Name'=>'Emirates Bank International',
        'bin'=>410685,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Alinma Bank',
        'bin'=>432328,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Alinma Bank',
        'bin'=>428671,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Alinma Bank',
        'bin'=>428672,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Alinma Bank',
        'bin'=>428673,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Alinma Bank',
        'bin'=>968206,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada'
    ],



    [
        'Bank_Name'=>'Alinma Bank',
        'bin'=>446672,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Alinma Bank',
        'bin'=>543357,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'Alinma Bank',
        'bin'=>434107,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'National Bank of Kuwait',
        'bin'=>431361,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'National Bank Of Bahrain',
        'bin'=>604906,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada'
    ],



    [
        'Bank_Name'=>'National Bank Of Bahrain',
        'bin'=>521076,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'National Commercial Bank',
        'bin'=>588850,
        'PAN_Length'=>'19',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'National Commercial Bank',
        'bin'=>968202,
        'PAN_Length'=>'19',
        'Card_Brand'=>'mada'
    ],



    [
        'Bank_Name'=>'National Commercial Bank',
        'bin'=>529415,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'National Commercial Bank',
        'bin'=>535825,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'National Commercial Bank',
        'bin'=>543085,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'National Commercial Bank',
        'bin'=>524130,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'National Commercial Bank',
        'bin'=>554180,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'National Commercial Bank',
        'bin'=>549760,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'Riyad Bank',
        'bin'=>588849,
        'PAN_Length'=>'18',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'Riyad Bank',
        'bin'=>968209,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada'
    ],



    [
        'Bank_Name'=>'Riyad Bank',
        'bin'=>524514,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'Riyad Bank',
        'bin'=>529741,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'Riyad Bank',
        'bin'=>537767,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'Riyad Bank',
        'bin'=>535989,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'Riyad Bank',
        'bin'=>536023,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'Riyad Bank',
        'bin'=>513213,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'Saudi American Bank',
        'bin'=>585265,
        'PAN_Length'=>'18',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'Saudi American Bank',
        'bin'=>588983,
        'PAN_Length'=>'18',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'Saudi American Bank',
        'bin'=>588982,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'Saudi American Bank',
        'bin'=>589005,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'Saudi American Bank',
        'bin'=>508160,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada'
    ],



    [
        'Bank_Name'=>'Saudi American Bank',
        'bin'=>531095,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'Saudi American Bank',
        'bin'=>530906,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'Saudi American Bank',
        'bin'=>532013,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'Saudi British Bank',
        'bin'=>588851,
        'PAN_Length'=>'17 / 19',
        'Card_Brand'=>'mada'
    ],



    [
        'Bank_Name'=>'Saudi British Bank',
        'bin'=>605141,
        'PAN_Length'=>'19',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'Saudi British Bank',
        'bin'=>968204,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada'
    ],



    [
        'Bank_Name'=>'Saudi British Bank',
        'bin'=>422817,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Saudi British Bank',
        'bin'=>422818,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Saudi British Bank',
        'bin'=>422819,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Saudi British Bank',
        'bin'=>428331,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Saudi Investment Bank',
        'bin'=>483010,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Saudi Investment Bank',
        'bin'=>483011,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Saudi Investment Bank',
        'bin'=>483012,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],



    [
        'Bank_Name'=>'Saudi Investment Bank',
        'bin'=>589206,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Mastercard co-branded'
    ],



    [
        'Bank_Name'=>'Saudi Investment Bank',
        'bin'=>968207,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada'
    ],
    [
        'Bank_Name'=>'Gulf International Bank',
        'bin'=>419593,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],
    [
        'Bank_Name'=>'Gulf International Bank',
        'bin'=>439956,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],

    [
        'Bank_Name'=>'Gulf International Bank',
        'bin'=>439954,
        'PAN_Length'=>'16',
        'Card_Brand'=>'mada / Visa co-branded'
    ],
];