<?php

return [
    'languages' => [
        'english' => env('PRESTA_EN_LANG_ID',1),
        'arabic' => env('PRESTA_AR_LANG_ID', 2)
    ],
    'shop_logo' => '/images/logo.png',
   // 'checkoutSecretKey' => env('CHECKOUT_SECRET_KEY','sk_test_c24f1f10-4f39-4c85-9e97-6df692729572'),
    'checkoutSecretKey' => env('CHECKOUT_SECRET_KEY','sk_84879806-b5d8-4749-9dfb-db22046b5696'),
    //'checkoutPublicKey' => env('CHECKOUT_PUBLIC_KEY','pk_test_5866e495-4c98-4c4a-b5c6-f82adffaa626'),
    'checkoutPublicKey' => env('CHECKOUT_PUBLIC_KEY','pk_8d664062-26b6-48df-be99-204ae55b1754'),
    'checkout_sandbox_mode' => env('CHECKOUT_SANDBOX_MODE',false),
    'payment_module' => [
        'ps_checkpayment' => 'ps_checkpayment',
        'codfee' => 'codfee',
        'checkoutapipayment' => 'checkoutapipayment',
    ],
];
