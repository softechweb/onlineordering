<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Configurations for uploaded content
    |--------------------------------------------------------------------------
    |
    */

    'slider_banners' => [
        'folder' => 'slider_banners/orignal',
        'rules' => [
            'mimes:jpeg,jpg,png',
            //'dimensions:min_width=753,min_height=271',
            'dimensions:min_width=750,min_height=271',
            'max:1024'
        ],
        
        'url' => [
            'folder' => 'slider_banners'
        ]
    ],

    'single_banner' => [
        'folder' => 'single_banner/orignal',
        'rules' => [
            'mimes:jpeg,jpg,png',
            //'dimensions:min_width=753,min_height=271',
            'dimensions:min_width=670,min_height=271',
            'max:1024'
        ],

        'url' => [
            'folder' => 'single_banner'
        ]
    ],

    'single_banner_2' => [
        'folder' => 'single_banner/orignal',
        'rules' => [
            'mimes:jpeg,jpg,png',
            'dimensions:min_width=670,min_height=271',
            'max:1024'
        ],

        'url' => [
            'folder' => 'single_banner'
        ]
    ],

    'single_banner_3' => [
        'folder' => 'single_banner/orignal',
        'rules' => [
            'mimes:jpeg,jpg,png',
            'dimensions:min_width=670,min_height=271',
            'max:1024'
        ],

        'url' => [
            'folder' => 'single_banner'
        ]
    ],

    'category_banners' => [
        'folder' => 'category_banners/orignal',
        'rules' => [
            'mimes:jpeg,jpg,png',
            //'dimensions:min_width=553,min_height=445',
            'dimensions:min_width=100,min_height=100',
            'max:1024'
        ],

        'url' => [
            'folder' => 'category_banners'
        ]
    ],

    'featured_banners' => [
        'folder' => 'featured_banners/orignal',
        'rules' => [
            'mimes:jpeg,jpg,png',
            'dimensions:min_width=553,min_height=445',
            'max:1024'
        ],

        'url' => [
            'folder' => 'featured_banners'
        ]
    ],

    'video_banner' => [
        'folder' => 'video_banner/orignal',
        'rules' => [
            'mimes:jpeg,jpg,png',
            'dimensions:max_width=2000,max_height=2000',
            'max:1024'
        ],

        'url' => [
            'folder' => 'video_banner'
        ]
    ],

    'detail_page_banner' => [
        'folder' => 'detail_page_banner/orignal',
        'rules' => [
            'mimes:jpeg,jpg,png',
            'dimensions:min_width=753,min_height=271',
            'max:1024'
        ],

        'url' => [
            'folder' => 'detail_page_banner'
        ]
    ],

    'top_categories' => [
        'folder' => 'category_banner/orignal',
        'rules' => [
            'mimes:jpeg,jpg,png',
            'dimensions:min_width=753,min_height=271',
            'max:1024'
        ],

        'url' => [
            'folder' => 'category_banner'
        ]
    ],

];
