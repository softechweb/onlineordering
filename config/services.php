<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_SES_ACCESS_KEY_ID', 'AKIAIQG46WB3OHHSV6PQ'),
        'secret' => env('AWS_SES_SECRET_ACCESS_KEY', 'Ao0s/H1VR7DJ8qzr6auD3pLHz795bu/MlAcOnVUsIdCb'),
        'region' => env('AWS_DEFAULT_REGION', 'eu-west-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'webhook' => [
            'secret' => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
    ],
    'google'=>[
        'client_id'=>env('GOOGLE_CLIENT_ID', '376062335211-8qhc6j4htkrntb3bnbmjt27eb7dnurug.apps.googleusercontent.com'),
        'client_secret'=>env('GOOGLE_CLIENT_SECRET', 'NZ6Q54mBgQgz90aQVkul4t9U'),
        'places_key'    =>env('GOOGLE_PLACES_KEY', 'AIzaSyBzlUrPPqs41UBhjKpXMhLsRZweT66-nFg'),
        'redirect'=>config('app.url')
    ],
    'facebook'=>[
        'client_id'=>env('FACEBOOK_CLIENT_ID', '626159624402559'),
        'client_secret'=>env('FACEBOOK_CLIENT_SECRET', '001bf708108a986f082cff84c74a838d'),
        'redirect'=>config('app.url')
    ]
];
