<?php

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('logout', 'Auth\LoginController@logout');
});

Route::group(['middleware' => ['scopes']], function () {

    /** User Routes */
    Route::get('user/view', 'Api\UserController@getAuthUser')->name('user.view');

    /** Update User Profile  **/
    Route::post('user/update/{id}', 'Api\UserController@updateUserProfile');
});