<?php

Route::group([
    'prefix' => 'auth'
], function () {

    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController@register');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.reset');
});

/*** All Roles on Admin listing ***/
Route::get('roles', 'Api\UserController@getRoles');

