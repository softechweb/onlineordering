<?php

return [
    'store' => 'Record created successfully.',
    'update' => 'Record updated successfully.',
    'destroy' => 'Record deleted successfully.',
    'success' => 'Data retrieved successfully.'
    ];

