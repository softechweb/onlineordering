
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/1999/REC-html401-19991224/strict.dtd">
<html>
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
</head>
<body>
<div style="background-color:#fff;width:100%;font-family:Open-sans, arial, sans-serif;color:#000;font-size:14px;line-height:18px;margin:auto">
    <table class="table table-mail" style="width:100%;margin-top:5px 0;;">
        <tbody>
        <tr>
            <td  style="padding:7px 0">
                <table class="table" bgcolor="#ffffff" style="width:100%">
                    <tbody>
                    <tr>
                        <td align="left" class="logo" style="padding:7px 0">
                            <a title="{{$shop_detail['name']}}" href="{{$shop_detail['domain']}}" style="color:#337ff1" target="_blank" data-saferedirecturl="">
                                <img src="{{$shop_detail['logo']}}" alt="{{$shop_detail['name']}}" data-image-whitelisted="" >
                            </a>
                        </td>
                        <td align="right" style="text-align:right;" class="invoice-code">
                            <p align="right" style="text-align: right; font-family: Open-sans, arial, sans-serif; font-size: 16px; margin: 0 0 5px 0;">
                                <strong>INVOICE</strong>
                            </p>
                            <p align="right" style="text-align: right; font-family: Open-sans, arial, sans-serif; margin: 0 0 2px 0;">
                                <span class="date">{{date('m/d/Y', strtotime($order['date_add']))}}</span>
                            </p>
                            <p align="right" style="text-align: right; font-family: Open-sans, arial, sans-serif; font-size: 16px; margin: 0 0 2px 0;">
                                <span class="ref-id">#IN{{$order['id_order']}}</span>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left" class="titleblock" style="padding:7px 0">
                            <font size="3" face="DejaVu Sans, sans-serif" color="#000">
                           <span style="font-family: DejaVu Sans, sans-serif;">من: وصول الغذائية للإتصالات و تقنية المعلومات
                           <br>الرقم الضريبي: ٣٠٠٧٧٤٨٦٣٢٠٠٠٠٣
                           </span>
                            </font>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="12">
                            <font  face="Open-sans, arial, sans-serif" color="#000">
                            <h3 style="margin-bottom: 0; font-size: 15px; text-align: left; font-weight: bold; color: #000; padding-bottom: 20px; display: inline-block;">
                                Contact Us:
                            </h3>
                            <span><strong style="margin: 0px; color: #000; text-align: left; font-family: Open-sans, arial, sans-serif; font-size: 15px; line-height: 18px;">Phone:</strong> +966-920007913 </span>&nbsp;&nbsp;
                            <span><strong style="margin: 0px; color: #000; text-align: left; font-family: Open-sans, arial, sans-serif; font-size: 15px; line-height: 18px;">Email: </strong> info@qavashop.com</span><br><br>
                            </font>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style=" height: 5px;"></td>
                    </tr>
                    <tr>
                        <td width="50%" class="box" style="padding:7px 0; vertical-align: top;">
                            <font  face="Open-sans, arial, sans-serif" color="#000">
                               <span style=" font-size: 15px; text-align: left; font-weight: bold; color: #000; padding-bottom: 20px; display: inline-block;">
                               Delivery Address
                               </span><br><br>
                                <span style="color: #000; text-align: left; font-family: Open-sans, arial, sans-serif; font-size: 15px; line-height: 18px;">
                               {{$delivery_address['firstname']}} {{$delivery_address['lastname']}}<br>
                               {!! isset($delivery_address['company']) ? $delivery_address['company'].' <br>' : '' !!}
                                {!! isset($delivery_address['vat_number']) ? $delivery_address['vat_number'].' <br>' : '' !!}
                               {{$delivery_address['address1']}}<br>
                               {{isset($delivery_address['country']['details'][0]['name']) ? $delivery_address['country']['details'][0]['name'] : ''}}<br>
                               {{$delivery_address['postcode']}} {{$delivery_address['city']}}<br>
                               {{$delivery_address['phone']}}
                               </span>
                            </font>
                        </td>
                        <td width="50%" class="box" style="padding:7px 0; vertical-align: top;">
                            <font  face="Open-sans, arial, sans-serif" color="#000">
                               <span style=" font-size: 15px; text-align: left; font-weight: bold; color: #000; padding-bottom: 20px; display: inline-block;">
                               Billing Address
                               </span><br><br>
                                <span style="color: #000; font-size: 15px; text-align: left; font-family: Open-sans, arial, sans-serif; line-height: 18px;">
                               {{$billing_address['firstname']}} {{$billing_address['lastname']}}<br>
                               {!! isset($billing_address['company']) ? $billing_address['company'].' <br>' : '' !!}
                                {!! isset($billing_address['vat_number']) ? $billing_address['vat_number'].' <br>' : '' !!}
                               {{$billing_address['address1']}}<br>
                               {{isset($billing_address['country']['details'][0]['name']) ? $billing_address['country']['details'][0]['name'] : ''}}<br>
                               {{$billing_address['postcode']}} {{$billing_address['city']}}<br>
                               {{$billing_address['phone']}}
                               </span>
                            </font>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style=" height: 5px;"></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table align="center" class="table table-recap" bgcolor="#ffffff" style="width:100%; text-align:center; border-collapse:collapse; border:1px solid #000;">
                                <tbody>
                                <tr>
                                    <th bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family: Open-sans, arial, sans-serif;font-size:13px;padding:5px 0;text-align: center;">Invoice Number</th>
                                    <th bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family: Open-sans, arial, sans-serif;font-size:13px;padding:5px 0;text-align: center;">Invoice Date </th>
                                    <th align="center" bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family: Open-sans, arial, sans-serif;font-size:13px;padding:5px;text-align: center;" width="18%">Order Reference</th>
                                    <th bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family: Open-sans, arial, sans-serif;font-size:13px;padding:5px 0;text-align: center;">Order date</th>
                                </tr>
                                <tr>
                                    <td align="center" style="background-color:#fff;color:#000;font-family: Open-sans, arial, sans-serif;font-size:12px;padding:5px 0;text-align: center;">
                                        #IN{{$order['id_order']}}
                                    </td>
                                    <td align="center" style="background-color:#fff;color:#000;font-family: Open-sans, arial, sans-serif;font-size:12px;padding:5px 0;text-align: center;">
                                        {{date('m/d/Y', strtotime($order['date_add']))}}
                                    </td>
                                    <td align="center" style="background-color:#fff;color:#000;font-family: Open-sans, arial, sans-serif;font-size:12px;padding:5px 0;text-align: center;">
                                        {{$order['reference']}}
                                    </td>
                                    <td align="center" style="background-color:#fff;color:#000;font-family: Open-sans, arial, sans-serif;font-size:12px;padding:5px 0;text-align: center;">
                                        {{date('m/d/Y', strtotime($order['date_add']))}}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style=" height: 5px;"></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table class="table table-recap" bgcolor="#ffffff" style="width:100%;border-collapse:collapse; border: 1px solid #000;">
                                <tbody>
                                <tr>
                                    <th bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family: Open-sans, arial, sans-serif;font-size:13px;padding:5px;text-align: center;">Reference</th>
                                    <th bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family: Open-sans, arial, sans-serif;font-size:13px;padding:5px 0;text-align: center;">Product </th>
                                    <th bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family: Open-sans, arial, sans-serif;font-size:13px;padding:5px 0;text-align: center;" width="17%">Tax Rate</th>
                                    <th bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family: Open-sans, arial, sans-serif;font-size:13px;padding:5px 0;text-align: center;">Unit Price (Tax excl.)</th>
                                    <th bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family: Open-sans, arial, sans-serif;font-size:13px;padding:5px 0;text-align: center;">Qty</th>
                                    <th bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family: Open-sans, arial, sans-serif;font-size:13px;padding:5px 0;text-align: center;">Total (Tax excl.)</th>
                                </tr>

                                @foreach($order_detail as $product)
                                    <tr>
                                        <td align="center" style="background-color:#fff;color:#000;font-family:Open-sans,Arial,sans-serif;font-size:11px;padding:5px 0;text-align: center;">
                                            {{$product['product_reference']}}
                                        </td>
                                        <td align="center" style="background-color:#fff;color:#000;font-family: Open-sans, arial, sans-serif;font-size:12px;padding:5px 0;text-align: center;">
                                            {{$product['product_name']}}
                                        </td>
                                        <td align="center" style="background-color:#fff;color:#000;font-family:Open-sans,Arial,sans-serif;font-size:12px;padding:5px 0;text-align: center;">
                                            {{--{{$product['reduction_percent']}} %--}}
                                            5.000 %
                                        </td>
                                        <td align="center" style="background-color:#fff;color:#000;font-family: Open-sans, arial, sans-serif;font-size:12px;padding:5px 0;text-align: center;">
                                            {{round((float)$product['unit_price_tax_excl'])}} SAR
                                        </td>
                                        <td align="center" style="background-color:#fff;color:#000;font-family: Open-sans, arial, sans-serif;font-size:12px;padding:5px 0;">
                                            {{$product['product_quantity']}}
                                        </td>
                                        <td align="center" style="background-color:#fff;color:#000;font-family: Open-sans, arial, sans-serif;font-size:12px;padding:5px 0;text-align: center;">
                                            {{round((float)$product['total_price_tax_excl'])}} SAR
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style=" height: 5px;"></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">
                            <table class="table table-recap" bgcolor="#ffffff" style="width:100%;border-collapse:collapse">
                                <tbody>
                                <tr>
                                    <td colspan="2">
                                        <table class="table table-recap" bgcolor="#ffffff" style="width:100%;border-collapse:collapse; border: 1px solid #000;">
                                            <tbody>
                                            <tr>
                                                <th bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family:Open-sans,Arial,sans-serif;font-size:13px;padding:5px 0; text-align: center;">Tax Detail</th>
                                                <th bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family:Open-sans,Arial,sans-serif;font-size:13px;padding:5px 0; text-align: center;">Tax Rate</th>
                                                <th bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family:Open-sans,Arial,sans-serif;font-size:13px;padding:5px 0; text-align: center;">Base price</th>
                                                <th bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family:Open-sans,Arial,sans-serif;font-size:13px;padding:5px 0; text-align: center;">Total Tax</th>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#fff;color:#000;font-family:Open-sans,Arial,sans-serif;font-size:12px;padding:5px 0; text-align: center;">
                                                    Products
                                                </td>
                                                <td align="center" style="background-color:#fff;color:#000;font-family:Open-sans,Arial,sans-serif;font-size:12px;padding:5px 0;text-align: center;">
                                                    5.000 %
                                                </td>
                                                <td align="center" style="background-color:#fff;color:#000;font-family:Open-sans,Arial,sans-serif;font-size:12px;padding:5px 0; text-align: center;">
                                                    {{ round($order['total_products']) }} SAR
                                                </td>
                                                <td align="center" style="background-color:#fff;color:#000;font-family:Open-sans,Arial,sans-serif;font-size:12px;padding:5px 0; text-align: center;">
                                                    {{(round($order['total_paid_tax_incl']) - round($order['total_paid_tax_excl']))}} SAR
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="space_footer" style="padding:0!important;height: 5px;">&nbsp;</td>
                                </tr>
                                <tr  class="conf_body">
                                    <td  bgcolor="#f0f0f0" style=" border-top: 1px solid #000; border-left: 1px solid #000; color:#000;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0"><span size="2" face="Open-sans, Arial, sans-serif" color="#000" style="color:#000;font-family:Open-sans,Arial,sans-serif;font-size:small; text-align: center; white-space: nowrap;"> <strong>Payment Method</strong> </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td bgcolor="#fff" align="right" style=" border-top: 1px solid #000; border-right: 1px solid #000;color:#000;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0"><span size="2" face="Open-sans, Arial, sans-serif" color="#000" style="color:#000;font-family:Open-sans,Arial,sans-serif;font-size:small;text-align: center; white-space: nowrap; "> {{$order['payment']}} {{round($order['total_paid'])}} SAR </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="conf_body">
                                    <td bgcolor="#f0f0f0" style="border-bottom: 1px solid #000; border-left: 1px solid #000;color:#000;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0"><span size="2" face="Open-sans, Arial, sans-serif" color="#000" style="color:#000;font-family:Open-sans,Arial,sans-serif;font-size:small;text-align: center;"> <strong>Carrier</strong> </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td bgcolor="#fff"  style="border-bottom: 1px solid #000; border-right: 1px solid #000;color:#000;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0"><span size="2" face="Open-sans, Arial, sans-serif" color="#000" style="color:#000;font-family: DejaVu Sans, sans-serif;font-size:small;text-align: center;"> {{$carrier_detail['name']}} </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>

                        <td style="vertical-align: top; padding-left: 50px;">
                            <table class="table" style="width:100%;border-collapse:collapse;">
                                <tbody>
                                <tr  class="conf_body">
                                    <td  bgcolor="#f0f0f0" style="color:#000;padding:7px 0;border-left: 1px solid #000; border-top: 1px solid #000;">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0;">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0"><span size="2" face="Open-sans, Arial, sans-serif" color="#000" style="color:#000;font-family:Open-sans,Arial,sans-serif;font-size:small"> Total Products </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td bgcolor="#fff" align="right" style="color:#000;padding:7px 0; border-right: 1px solid #000; border-top: 1px solid #000;">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0;"><span size="2" face="Open-sans, Arial, sans-serif" color="#000" style="color:#000;font-family:Open-sans,Arial,sans-serif;font-size:small"> {{ round($order['total_products']) }} SAR </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr  class="conf_body">
                                    <td  bgcolor="#f0f0f0" style="color:#000;padding:7px 0; border-left: 1px solid #000;">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0;"><span size="2" face="Open-sans, Arial, sans-serif" color="#000" style="color:#000;font-family:Open-sans,Arial,sans-serif;font-size:small"> Shipping Costs </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td bgcolor="#fff" align="right" style="color:#000;padding:7px 0; border-right: 1px solid #000;">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0"><span size="2" face="Open-sans, Arial, sans-serif" color="#000" style="color:#000;font-family:Open-sans,Arial,sans-serif;font-size:small"> {{round($order['total_shipping'])}} SAR </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr  class="conf_body">
                                    <td  bgcolor="#f0f0f0" style="color:#000;padding:7px 0; border-left: 1px solid #000;">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0"><span size="2" face="Open-sans, Arial, sans-serif" color="#000" style="color:#000;font-family:Open-sans,Arial,sans-serif;font-size:small"> <strong>Total (Tax excl.)</strong> </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td bgcolor="#fff" align="right" style="color:#000;padding:7px 0; border-right: 1px solid #000;">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0"><span size="2" face="Open-sans, Arial, sans-serif" color="#000" style="color:#000;font-family:Open-sans,Arial,sans-serif;font-size:small"><strong>{{round($order['total_paid_tax_excl'])}} SAR</strong> </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr  class="conf_body">
                                    <td  bgcolor="#f0f0f0" style="color:#000;padding:7px 0; border-left: 1px solid #000;">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0"><span size="2" face="Open-sans, Arial, sans-serif" color="#000" style="color:#000;font-family:Open-sans,Arial,sans-serif;font-size:small"> <strong>Total Tax</strong> </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td bgcolor="#fff" align="right" style="color:#000;padding:7px 0; border-right: 1px solid #000;">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0"><span size="2" face="Open-sans, Arial, sans-serif" color="#000" style="color:#000;font-family:Open-sans,Arial,sans-serif;font-size:small"><strong>{{(round($order['total_paid_tax_incl']) - round($order['total_paid_tax_excl']))}} SAR</strong> </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr  class="conf_body">
                                    <td  bgcolor="#f0f0f0" style="color:#000;padding:7px 0; border-left: 1px solid #000; border-bottom: 1px solid #000;">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0"><span size="2" face="Open-sans, Arial, sans-serif" color="#000" style="color:#000;font-family:Open-sans,Arial,sans-serif;font-size:small"> <strong>Total</strong> </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td bgcolor="#fff" align="right" style="color:#000;padding:7px 0; border-right: 1px solid #000; border-bottom: 1px solid #000;">
                                        <table class="table" style="width:100%;border-collapse:collapse">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0"><span size="2" face="Open-sans, Arial, sans-serif" color="#000" style="color:#000;font-family:Open-sans,Arial,sans-serif;font-size:small"><strong>{{round($order['total_paid'])}} SAR</strong> </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" align="center" class="footer" style=" text-align: center; padding:20px 0 8px">
                            <span style="font-size: 11px; color: #444444; font-family: Open-sans, arial, sans-serif;">QavaShop - Saudi Arabia</span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>