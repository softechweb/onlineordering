<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>{{config('app.name')}} - Magic Link</title>
</head>
<body style="margin: 0;padding:0;">
<style>
    @import url('https://fonts.googleapis.com/css?family=Montserrat:400,700');
</style>
<div class="email-wrapper" style="width:100%;overflow:hidden;background-color: #f5f7f7;font-size:12px;padding: 60px 0;">
    <table class="main-table" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;width:100%;margin: 0 auto;background-color: #ffffff;font-family: 'Montserrat', sans-serif;text-align: left;">
        <tr>
            <td align="left" valign="middle" style="padding: 0 25px;">
                <div class="top-header">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto;background-color: #ffffff;font-family: 'Montserrat', sans-serif;text-align: left;">
                        <tr>
                            <td align="left" valign="middle" style="padding: 15px 0 0px;text-align: center;"><img src="{{config('app.app_logo')}}" alt="{{config('app.name')}} Logo" width="160" style="max-width: 100%;"></td>
                        </tr>
                        <tr>
                            <td style="padding: 0 0;"><hr style="margin: 0;border-color: #eff2fc;border-top: 0;"></td>
                        </tr>
                    </table>
                </div> <!--logo area end-->

                <div class="body-section section1">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto;background-color: #ffffff;font-family: 'Montserrat', sans-serif;text-align: left;">
                        <tr>
                            <td align="left" valign="middle" style="padding: 20px 0;">
                                <table  border="0" cellpadding="0" cellspacing="0" style="width:100%;text-align: left;">
                                    <tr>
                                        <td colspan="2" style="padding: 0 60px;">
                                            <div class="order-number-table">
                                                <table border="0" cellpadding="0" cellspacing="0" style="width:100%;text-align: center;">
                                                    <tbody>
                                                    <tr>
                                                        <td width="100%" style="padding:20px 0;">
                                                            <p style="margin:0;font-size: 18px;line-height: 24px;">Click the Authenticate button to continue.</p>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <table border="0" cellpadding="0" cellspacing="0" style="width:100%;text-align: center;border-bottom: 0;">
                                                    <tbody>
                                                    <tr>
                                                        <td align="center" valign="top" style="padding:10px;color:#1a1a1a;font-size: 22px;">
                                                            <a href="{{$link}}" style="display: inline-block;text-decoration: none;background-color: #00d585;color:#fff;font-size: 16px;text-transform: uppercase;padding:17px 30px 15px;line-height: 1;font-weight: 700;border-radius: 4px;">AUTHENTICATE</a>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div> <!--body top area end-->
            </td>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;width:100%;margin: 0 auto;background-color: #333333;font-family: 'Montserrat', sans-serif;text-align: left;color:#ffffff;width: 100%;">
        <tr>
            <td align="left" valign="middle" style="padding: 15px 40px;">
                <div class="body-footer">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0;background-color: #333333;font-family: 'Montserrat', sans-serif;text-align: left;width: 100%;">
                        <tbody>
                        <tr>
                            <td align="center" valign="middle" style="padding:0;">
                                <p style="margin: 0;color: #999999;">© 2019 - Powered by {{config('app.name')}}. All Rights Reserved</p>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>
</body>
</html>