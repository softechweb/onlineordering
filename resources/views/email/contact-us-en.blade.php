<div style="background-color: #fff; width: 100%;">
<div style="background-color:#fff;width:650px;font-family:Open-sans,arial,sans-serif;color:#555454;font-size:13px;line-height:18px;margin:auto">
    <table class="table table-mail" style="width:100%;margin-top:10px">
        <tbody>
        <tr>
            <td class="space" style="width:20px;padding:7px 0">&nbsp;</td>
            <td align="center" style="padding:7px 0">
                <table class="table" bgcolor="#ffffff" style="width:100%">
                    <tbody>
                    <tr>
                        <td align="center" class="logo" style="border-bottom:4px solid #333333;padding:7px 0">
                            <a title="{{$data['shop_detail']['name']}}" href="{{$data['shop_detail']['domain']}}" style="color:#337ff1" target="_blank" data-saferedirecturl="">
                                <img src="{{$data['shop_detail']['logo']}}" alt="{{$data['shop_detail']['name']}}" data-image-whitelisted="">
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="titleblock" style="padding:7px 0">
                            <font size="2" face="Open-sans, arial, sans-serif" color="#555454">
                                <span class="title" style="font-weight:500;font-family:Open-sans, arial, sans-serif;font-size:28px;text-transform:uppercase;line-height:33px">MESSAGE FROM A QAVASHOP CUSTOMER,</span>
                            </font>
                        </td>
                    </tr>
                    <tr>
                        <td class="space_footer" style="padding:0!important">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="box" style="border:1px solid #d6d4d4;background-color:#f8f8f8;padding:7px 0">
                            <table class="table" style="width:100%; margin:0 auto;">
                                <tbody>
                                <tr>
                                    <td width="10" style="padding:7px 0">&nbsp;</td>
                                    <td style="padding:7px 0">
                                        <font size="2" face="Open-sans, arial, sans-serif" color="#555454">
                                            </p>
                                            <span style="color:#777;font-family:Open-sans, arial, sans-serif;">
                                                <strong><span style="color:#333">Customer e-mail address:</span></strong>
                                                {{$data['customer_email']}}
                                                <strong><span style="color:#333;font-family:Open-sans, arial, sans-serif;">Customer message:</span></strong>.
                                                {{$data['message']}}<br>
                                          </span>
                                        </font>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td class="space_footer" style="padding:0!important">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="footer" style="border-top:4px solid #333333;padding:7px 0;font-family:Open-sans, arial, sans-serif;">
                            <span>
                                <a href="{{$data['shop_detail']['domain']}}" style="color:#337ff1" target="">{{$data['shop_detail']['name']}}</a>
                            </span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td class="space" style="width:20px;padding:7px 0">&nbsp;</td>
        </tr>
        </tbody>
    </table>
</div>
</div>