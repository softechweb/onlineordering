<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Message from {{$data['shop_detail']['name']}}</title>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Montserrat:400,600,700&display=swap');
        div *{
            font-family: 'Montserrat', sans-serif !important;
        }
        @media (max-width: 767px){
            .order-number-table tr td{
                font-size: 12px !important;
            }
            .email-wrapper {
                padding: 30px 0 !important;
            }
            .main-table {
                width: 90% !important;
            }

        }
    </style>
</head>
<body style="margin: 0;padding:0;">
<div class="email-wrapper" style="width:100%;overflow:hidden;background-color: #f5f7f7;font-size:12px;padding: 60px 0;">
    <table class="main-table" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;width:100%;margin: 0 auto;background-color: #ffffff;font-family: Open-sans, sans-serif;text-align: left;">
        <tr>
            <td align="left" valign="middle" style="padding: 0 0px;">
                <div class="top-header">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto;background-color: #ffffff;font-family: Open-sans, sans-serif;text-align: left;">
                        <tr>
                            <td align="left" valign="middle" style="padding: 28px 0 23px;text-align: center;"><img src="{{env('APP_LOGO_AR','https://d2z3i2y6z8pv3i.cloudfront.net/img/cms/logo-ar.png')}}" alt="{{$data['shop_detail']['name']}}"  style="max-width: 100%;"></td>
                        </tr>
                        <tr>
                            <td style="padding: 0 0;"><hr style="margin: 0;border-color: #eff2fc;border-top: 0;"></td>
                        </tr>
                    </table>
                </div> <!--logo area end-->
                <div class="body-top " style="padding: 0 30px;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin: 0 auto;background-color: #ffffff;font-family: Open-sans, sans-serif;text-align: center;">
                        <tr>
                            <td  valign="middle" style="padding: 20px 0 47px;">
                                <h2 style="font-size:36px; color: #000000; margin:0;  text-align: center;"><strong>اختياراتك الرائعة من المنتجات في انتظارك</strong></h2>
                                {{--<p style="font-size:18px; color:#585858; margin:13px 0 0; ">اختياراتك الرائعة من المنتجات في انتظارك</p>--}}
                            </td>
                        </tr>
                    </table>
                </div> <!--body top area end-->
                <div class="main-body" style="padding: 0 30px 40px;">
                    <div style="background-color: #e7ecef; padding: 15px 25px 30px; border-radius: 8px;position: relative;direction: rtl;text-align: right;">
                        <span style="width: 0; height: 0; border-left: 30px solid transparent;border-right: 30px solid transparent;border-bottom: 30px solid#e7ecef;position: absolute;top: -30px;left: 0;right: 0;margin: 0 auto;"></span>
                        <ul style="list-style: none;padding: 0;margin: 0;">
                            @foreach($data['cart']['cart_items'] as $product)
                                <li style="border-bottom: 1px solid#cccccc;padding:17px 0;">
                                    {{--<span  style="background-image: {{$product['product']['details'][0]['image_url']}};display: inline-block;width:80px;height: 80px;background-repeat: no-repeat; background-size: contain; background-position: center; background-color:#fff;  margin-left: 5px;"></span>--}}
                                    <img src="{{$product['product']['details'][0]['image_url']}}" style="display: inline-block;width:80px;height: 80px;background-repeat: no-repeat; background-size: contain; background-position: center; background-color:#fff;  margin-right: 5px;">
                                    <div style="width: calc(100% - 98px);display: inline-block;vertical-align: top;margin-right: 9px;text-align: right;">
                                        <p style="color: #000000; font-size: 14px;margin: 3px 0 14px; text-align: right;">{{$product['product']['details'][0]['name']}}</p>
                                        <strong style="color: #37b4b6; font-size: 16px;">{{ number_format(round($product['product']['final_price_with_vat'])) }} ريال </strong>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                        <div>
                            <a href="{{Config::get('app.BASE_URL').app()->getLocale().'/order?step=3&recover_cart='.$data['cart']['id_cart'].'&token_cart='.md5(config('app.presta_cookie_key') . 'recover_cart_' . $data['cart']['id_cart'])}}" style="padding: 15px 0px 17px;width: 100%; font-size: 20px;background-color: #39b54a;border-radius: 5px;font-weight: 600;color: #fff;text-decoration: none;display: inline-block;text-align: center;margin-top: 37px;">متابعة التسوّق</a>
                        </div>
                    </div>
                    <div class="cnt-detail">
                        <p style="font-weight: 700;font-size: 18px;color: #000000;text-align: center;margin: 60px 0 7px;">هل تحتاج أية مساعدة لإكمال طلبك؟</p>
                        <p style="font-size: 18px;color: #888888;text-align: center;margin: 0px 0 0;">يرجى التواصل مع مركز خدمة العملاء</p>
                        <ul style="text-align: center;list-style: none;padding: 50px 0 0;margin: 0;">
                            <li style="background-color: #fffbc2;border: 1px solid#eee9a5;padding: 9px 40px;border-radius: 3px;display: inline-block;margin: 0 7px 5px;">
                                <span style="font-size: 9px;text-transform: uppercase;font-weight: 700;color:#000; display: block;margin-bottom:3px; ">اتصل على</span>
                                <a href="tel:+920007913" style="color: #66bfc2; font-size: 12px; font-weight: 700;">920007913</a>
                            </li>
                            <li style="background-color: #fffbc2;border: 1px solid#eee9a5;padding: 9px 20px;border-radius: 3px;display: inline-block;margin: 0 7px 5px;">
                                <span style="font-size: 9px;text-transform: uppercase;font-weight: 700;color:#000; display: block;margin-bottom:3px; ">البريد الإلكتروني</span>
                                <a href="mailto:{{config('mail.from.address')}}" style="color: #66bfc2; font-size: 12px; font-weight: 700;">{{config('mail.from.address')}}</a>
                            </li>
                            <li style="background-color: #fffbc2;border: 1px solid#eee9a5;padding: 9px 40px;border-radius: 3px;display: inline-block;margin: 0 7px 5px;">
                                <span style="font-size: 9px;text-transform: uppercase;font-weight: 700;color:#000; display: block;margin-bottom:3px; ">محادثة</span>
                                <a href="{{Config::get('app.BASE_URL').app()->getLocale()}}" style="color: #66bfc2; font-size: 12px; font-weight: 700;">اضغط لبدء المحادثة</a>
                            </li>
                        </ul>
                    </div>
                </div> <!--body area end-->
                {{--<p style="text-align: center;background-color: #f5f7f7;margin: 0;padding: 25px 0 0;background-image: url(images/shadow-img.png);background-repeat: no-repeat;background-size: contain;"><a href="#" style="font-size: 14px;color: #8c8c8c;text-decoration: none;">Unsubscribe etc.</a> </p>--}}
            </td>
        </tr>
    </table>

</div>
</body>
</html>
