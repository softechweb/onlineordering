<div style="width:100%;background-color: #fff;">
    <div style="background-color:#fff;width:650px;font-family:Open-sans,arial,sans-serif;color:#555454;font-size:13px;line-height:18px;margin:auto">
    <table class="table table-mail" style="width:100%;margin-top:10px;direction:rtl;">
        <tbody>
        <tr>
            <td class="space" style="width:20px;padding:7px 0">&nbsp;</td>
            <td align="center" style="padding:7px 0">
                <table class="table" bgcolor="#ffffff" style="width:100%">
                    <tbody>
                    <tr>
                        <td align="center" class="logo" style="border-bottom:4px solid #333333;padding:7px 0">
                            <a title="{{$data['shop_detail']['name']}}" href="{{$data['shop_detail']['domain']}}" style="color:#337ff1" target="_blank" data-saferedirecturl="">
                                <img src="{{$data['shop_detail']['logo']}}" alt="{{$data['shop_detail']['name']}}" data-image-whitelisted="">
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="titleblock" style="padding:7px 0">
                            <font size="2" face="Open-sans, arial, sans-serif" color="#555454">
                                <span class="title" style="font-family: Open-sans, sans-serif;font-size:28px;text-transform:uppercase;line-height:33px">مرحبا {{$data['user_detail']['firstname']}} {{$data['user_detail']['lastname']}},</span>
                            </font>
                        </td>
                    </tr>
                    <tr>
                        <td class="space_footer" style="padding:0!important">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="box" style="border:1px solid #d6d4d4;background-color:#f8f8f8;padding:7px 0">
                            <table class="table" style="width:100%; margin:0 auto;">
                                <tbody>
                                <tr>
                                    <td width="10" style="padding:7px 0">&nbsp;</td>
                                    <td style="padding:7px 0">
                                        <font align="right" size="2" face="Open-sans, arial, sans-serif" color="#555454">
                                            <p style="border-bottom:1px solid #d6d4d4; text-align:right;margin:0px 0 7px;text-transform:uppercase;font-weight:500;font-size:18px;padding-bottom:10px">
                                            خطأ في عملية الدفع {{$data['order']['reference']}}&nbsp;-&nbsp;طلب
                                            </p>
                                            <span align="right" style="color:#777; display: inline-block; width: 100%; text-align: right;">الرجاء الاتصال بنا بأقرب فرصة ممكنة
                                                <strong><span style="color:#333">{{$data['shop_detail']['name']}}</span></strong>
                                                الرقم المرجعي للطلب
                                                <strong><span style="color:#333">{{$data['order']['reference']}}</span></strong>.
                                                توجد لديك مشكله في الدفع الى
                                                <strong><span style="color:#333">سيتم شحن الطلب الخاص بك بعد تحصيل قيمة الطلب</span></strong>
                                          </span>
                                        </font>
                                    </td>
                                    <td width="10" style="padding:7px 0">&nbsp;</td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="space_footer" style="padding:0!important">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="linkbelow" style="padding:7px 0; text-align: right;">
                            <font align="right" size="2" face="Open-sans, arial, sans-serif" color="#555454">
                           <span>
                            يمكنك متابعة طلبك وتحميل الفاتورة من
                               <a href="{{url(app()->getLocale().'/order-history')}}" style="color:#337ff1" target="">"سجل الطلبيات"</a>
                               من حساب العملاء عن طريق النقرعلى
                               <a href="{{url(app()->getLocale().'/my-account')}}" style="color:#337ff1" target="">"حسابي"</a>
                               على موقعنا
                           </span>
                            </font>
                        </td>
                    </tr>
                    <tr>
                        <td class="linkbelow" style="padding:7px 0; text-align: right;">
                            <font size="2" face="Open-sans, arial, sans-serif" color="#555454">
                           <span>
                          
إذا كان لديك حساب الزوار، يمكنك متابعة طلبيتك عن طريق
                               <a href="{{url(app()->getLocale().'/guest-tracking?id_order='.$data['order']['reference'])}}" style="color:#337ff1" target="_blank" data-saferedirecturl=""> "ميزة تتبع الزوار"</a>
                               في القسم الخاص على موقعنا
                           </span>
                            </font>
                        </td>
                    </tr>
                    <tr>
                        <td class="space_footer" style="padding:0!important">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="footer" style="border-top:4px solid #333333;padding:7px 0;text-align: center;">
                            <span>
                                <a href="{{$data['shop_detail']['domain']}}" style="color:#337ff1" target="">{{$data['shop_detail']['name']}}</a>
                            </span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td class="space" style="width:20px;padding:7px 0">&nbsp;</td>
        </tr>
        </tbody>
    </table>
</div>
</div>
