<div style="width:100%;background-color:#fff;">
    <div style="background-color:#fff;width:650px;font-family:Open-sans, arial, sans-serif;color:#555454;font-size:13px;line-height:18px;margin:auto">
        <table class="table table-mail" style="width:100%;margin-top:10px;direction: rtl;">
        <tbody>
        <tr>
            <td class="space" style="width:20px;padding:7px 0">&nbsp;</td>
            <td align="center" style="padding:7px 0">
                <table class="table" bgcolor="#ffffff" style="width:100%">
                    <tbody>
                    <tr>
                        <td align="center" class="logo" style="border-bottom:4px solid #333333;padding:7px 0">
                            <a title="{{$data['shop_detail']['name']}}" href="{{$data['shop_detail']['domain']}}" style="color:#337ff1" target="_blank" data-saferedirecturl="">
                                <img src="{{$data['shop_detail']['logo']}}" alt="{{$data['shop_detail']['name']}}" data-image-whitelisted="" class="CToWUd">
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="titleblock" style="padding:7px 0">
                            <span size="2" face="Open-sans, arial, sans-serif" color="#555454" style="color:#555454;font-family:Open-sans, arial, sans-serif;font-size:small">
                                <span class="title" style="font-family: Open-sans, sans-serif;font-size:28px;text-transform:uppercase;line-height:33px">مرحبا {{$data['user_detail']['firstname']}} {{$data['user_detail']['lastname']}},</span>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="space_footer" style="padding:0!important">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="box" style="border:1px solid #d6d4d4;background-color:#f8f8f8;padding:7px 0">
                            <table class="table" style="width:100%; margin:0 auto;">
                                <tbody>
                                <tr>
                                    <td width="10" style="padding:7px 0">&nbsp;</td>
                                    <td style="padding:7px 0; text-align: right;">
                                        <p style="border-bottom:1px solid #d6d4d4;margin:0px 0 7px;text-transform:uppercase;font-weight:500;font-size:18px;padding-bottom:10px;text-align: right;">
                                            قيد المعالجة {{$data['order']['reference']}}&nbsp;-&nbsp;طلب
                                        </p>
                                        <span size="2" face="Open-sans, arial, sans-serif" color="#555454" style="color:#555454;font-family:Open-sans, arial, sans-serif;font-size:small">
                                            <span style="color:#777"> نحن حاليا نجهز طلبك من
                                                <strong><span style="color:#333">{{$data['shop_detail']['name']}}</span></strong>
                                                ذو الرقم المرجعي للطلب
                                                <strong><span style="color:#333">{{$data['order']['reference']}}</span></strong>.
                                            </span>
                                        </span>
                                    </td>
                                    <td width="10" style="padding:7px 0">&nbsp;</td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="space_footer" style="padding:0!important">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="linkbelow" style="padding:7px 0; text-align: right;">
                            <span size="2" face="Open-sans, arial, sans-serif" color="#555454" style="color:#555454;font-family:Open-sans, arial, sans-serif;font-size:small">
                                <span>
                                    يمكنك متابعة طلبك وتحميل الفاتورة من
                                    <a href="{{Config::get('app.BASE_URL').app()->getLocale().'/order-history'}}" style="color:#337ff1" target="_blank" data-saferedirecturl="">"سجل الطلبيات"</a>
                                    من حساب العملاء عن طريق النقرعلى
                                    <a href="{{Config::get('app.BASE_URL').app()->getLocale().'/my-account'}}" style="color:#337ff1" target="_blank" data-saferedirecturl="">"حسابي" </a>
                                    على موقعنا
                                </span>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="linkbelow" style="padding:7px 0; text-align: right;">
                            <span size="2" face="Open-sans, arial, sans-serif" color="#555454" style="color:#555454;font-family:Open-sans, arial, sans-serif;font-size:small">
                                <span>
                                    
إذا كان لديك حساب الزوار، يمكنك متابعة طلبيتك عن طريق
                                    <a href="{{Config::get('app.BASE_URL').app()->getLocale().'/guest-tracking?id_order='.$data['order']['reference']}}" style="color:#337ff1" target="_blank" data-saferedirecturl=""> "ميزة تتبع الزوار"</a> في القسم الخاص على موقعنا
                                </span>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="space_footer" style="padding:0!important">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="footer" style="border-top:4px solid #333333;padding:7px 0; text-align: center;">
                            <span>
                                <a href="{{$data['shop_detail']['domain']}}" style="color:#337ff1" target="_blank" data-saferedirecturl="">{{$data['shop_detail']['name']}}</a>
                            </span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td class="space" style="width:20px;padding:7px 0">&nbsp;</td>
        </tr>
        </tbody>
    </table>

    </div>
</div>