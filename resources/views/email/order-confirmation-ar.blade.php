<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width">
</head>
<body>
<div style="width:100%;background-color:#fff;">
    <div style="background-color:#fff;width:650px;font-family:Open-sans,Arial,sans-serif;color:#555454;font-size:13px;line-height:18px;margin:auto">
    <table class="" style="width:100%;margin-top:10px;direction: rtl;">
        <tbody>
        <tr>
            <td style="width:20px;padding:7px 0">&nbsp;</td>
            <td align="center" style="padding:7px 0">
                <table bgcolor="#ffffff" style="width:100%">
                    <tbody>
                    <tr>
                        <td align="center" class="logo" style="border-bottom:4px solid #333333;padding:7px 0">
                            <a title="{{$data['shop_detail']['name']}}" href="{{$data['shop_detail']['domain']}}"
                               style="color:#337ff1" target="_blank" data-saferedirecturl="">
                                <img src="{{$data['shop_detail']['logo']}}" alt="{{$data['shop_detail']['name']}}"
                                     data-image-whitelisted="">
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="titleblock" style="padding:7px 0">
                            <span size="2" face="Open-sans, sans-serif" color="#555454"
                                  style="color:#555454;font-family:Open-sans,Arial,sans-serif;font-size:small">
                                <span class="title"
                                      style="font-family: Open-sans, sans-serif;font-size:28px;text-transform:uppercase;line-height:33px">مرحبا {{$data['user_detail']['firstname']}} {{$data['user_detail']['lastname']}},</span><br>
                                <span class="subtitle"
                                      style="font-weight:400;font-size:16px;text-transform:uppercase;line-height:25px"> شكرا لتسوقك في! {{$data['shop_detail']['name']}}</span>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="space_footer" style="padding:0!important">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="box" style="border:1px solid #d6d4d4;background-color:#f8f8f8;padding:7px 0">
                            <table class="table" style="width:100%">
                                <tbody>
                                <tr>
                                    <td width="10" style="padding:7px 0">&nbsp;</td>
                                    <td align="right" style="padding:7px 0; text-align: right;">
                                        <p style="border-bottom:1px solid #d6d4d4;margin:0px 0 7px;text-transform:uppercase;font-weight:500;font-size:18px;padding-bottom:10px;text-align:right;">
                                            تفاصيل الطلب</p>
                                        <p style="color:#555454;margin:0;padding:0;font-family:Open-sans,Arial,sans-serif;text-align: right;"> <strong style="color:#333"> الطلب:</strong> <span>{{$data['order']['reference']}} وضع في {{date('d/m/Y h:i:s', strtotime($data['order']['date_add']))}}</span></p><p style="color:#555454;margin:0;padding:0;font-family:Open-sans,Arial,sans-serif;text-align: right;"> <strong style="color:#333">أسلوب الدفع:</strong> <span>{{$data['order']['payment']}}</span></p>
                                    </td>
                                    <td width="10" style="padding:7px 0">&nbsp;</td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding:7px 0">
                            <table class="table table-recap" bgcolor="#ffffff"
                                   style="width:100%;border-collapse:collapse">
                                <tbody>
                                <tr>
                                    <th bgcolor="#f8f8f8"
                                        style="border:1px solid #d6d4d4;white-space: nowrap;background-color:#fbfbfb;color:#333;font-family:Arial;font-size:13px;padding:10px">
                                        المرجع
                                    </th>
                                    <th bgcolor="#f8f8f8"
                                        style="border:1px solid #d6d4d4;white-space: nowrap;background-color:#fbfbfb;color:#333;font-family:Arial;font-size:13px;padding:10px">
                                        منتج
                                    </th>
                                    <th bgcolor="#f8f8f8"
                                        style="border:1px solid #d6d4d4;white-space: nowrap;background-color:#fbfbfb;color:#333;font-family:Arial;font-size:13px;padding:10px"
                                        width="17%">سعر الوحدة	
                                    </th>
                                    <th bgcolor="#f8f8f8"
                                        style="border:1px solid #d6d4d4;white-space: nowrap;background-color:#fbfbfb;color:#333;font-family:Arial;font-size:13px;padding:10px">
                                        الكمية
                                    </th>
                                    <th bgcolor="#f8f8f8"
                                        style="border:1px solid #d6d4d4;white-space: nowrap;background-color:#fbfbfb;color:#333;font-family:Arial;font-size:13px;padding:10px"
                                        width="17%">السعر الإجمالي
                                    </th>
                                </tr>
                                <tr>
                                    <td colspan="5"
                                        style="border:1px solid #d6d4d4;text-align:center;color:#777;padding:7px 0">
                                        &nbsp;&nbsp;
                                    </td>
                                </tr>

                                @foreach($data['order_detail'] as $product)
                                    <tr>
                                        <td style="border:1px solid #d6d4d4">
                                            <table class="table" style="margin:0 auto;">
                                                <tbody>
                                                <tr>
                                                    <td width="10">&nbsp;</td>
                                                    <td style="padding:0;">
                                                        <font size="2" face="Open-sans, Arial, sans-serif"
                                                              color="#555454" style="white-space: nowrap;">
                                                            {{$product['product_reference']}}
                                                        </font>
                                                    </td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td style="border:1px solid #d6d4d4">
                                            <table class="table" style="margin:0 auto;>
                                                <tbody>
                                                <tr>
                                                    <td width="10">&nbsp;</td>
                                                    <td style="padding:0;">
                                                        <font size="2" face="Open-sans, Arial, sans-serif"
                                                              color="#555454">
                                                            <strong>{{$product['product_name']}}</strong>
                                                        </font>
                                                    </td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td style="border:1px solid #d6d4d4">
                                            <table class="table" style="margin:0 auto;>
                                                <tbody>
                                                <tr>
                                                    <td width="10">&nbsp;</td>
                                                    <td style="padding:0;">
                                                        <font size="2" face="Open-sans, Arial, sans-serif"
                                                              color="#555454">
                                                            {{ round($product['unit_price_tax_excl']) }} SAR
                                                        </font>
                                                    </td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td style="border:1px solid #d6d4d4">
                                            <table class="table" style="margin:0 auto;>
                                                <tbody>
                                                <tr>
                                                    <td width="10">&nbsp;</td>
                                                    <td style="padding:0;">
                                                        <font size="2" face="Open-sans, Arial, sans-serif"
                                                              color="#555454">
                                                            {{$product['product_quantity']}}
                                                        </font>
                                                    </td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                        <td style="border:1px solid #d6d4d4">
                                            <table class="table" style="margin:0 auto;>
                                                <tbody>
                                                <tr>
                                                    <td width="10">&nbsp;</td>
                                                    <td style="padding:0;">
                                                        <font size="2" face="Open-sans, Arial, sans-serif"
                                                              color="#555454">
                                                            {{ round($product['total_price_tax_excl']) }} SAR
                                                        </font>
                                                    </td>
                                                    <td width="10">&nbsp;</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                @endforeach

                                <tr>
                                    <td colspan="5"
                                        style="border:1px solid #d6d4d4;text-align:center;color:#777;padding:7px 0">
                                        &nbsp;&nbsp;
                                    </td>
                                </tr>
                                <tr class="conf_body">
                                    <td bgcolor="#f8f8f8" colspan="4"
                                        style="border:1px solid #d6d4d4;color:#333;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse; margin:0 auto;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#333;padding:0"><span size="2"
                                                                                                     face="Open-sans, Arial, sans-serif"
                                                                                                     color="#555454"
                                                                                                     style="color:#555454;font-family:Open-sans,Arial,sans-serif;font-size:small"> <strong>المنتجات</strong> </span>
                                                </td>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td bgcolor="#f8f8f8" align="right" colspan="4"
                                        style="border:1px solid #d6d4d4;color:#333;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse; margin:0 auto;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#333;padding:0"><span size="2"
                                                                                                     face="Open-sans, Arial, sans-serif"
                                                                                                     color="#555454"
                                                                                                     style="color:#555454;font-family:Open-sans,Arial,sans-serif;font-size:small"> {{ round($data['order']['total_products']) }} SAR </span>
                                                </td>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="conf_body">
                                    <td bgcolor="#f8f8f8" colspan="4"
                                        style="border:1px solid #d6d4d4;color:#333;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse;margin:0 auto;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#333;padding:0"><span size="2"
                                                                                                     face="Open-sans, Arial, sans-serif"
                                                                                                     color="#555454"
                                                                                                     style="color:#555454;font-family:Open-sans,Arial,sans-serif;font-size:small"> <strong>خصومات</strong> </span>
                                                </td>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td bgcolor="#f8f8f8" colspan="4"
                                        style="border:1px solid #d6d4d4;color:#333;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse;margin:0 auto;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#333;padding:0"><span size="2"
                                                                                                     face="Open-sans, Arial, sans-serif"
                                                                                                     color="#555454"
                                                                                                     style="color:#555454;font-family:Open-sans,Arial,sans-serif;font-size:small"> {{ round($data['order']['total_discounts']) }} SAR </span>
                                                </td>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="conf_body">
                                    <td bgcolor="#f8f8f8" colspan="4"
                                        style="border:1px solid #d6d4d4;color:#333;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse;margin:0 auto;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#333;padding:0"><span size="2"
                                                                                                     face="Open-sans, Arial, sans-serif"
                                                                                                     color="#555454"
                                                                                                     style="color:#555454;font-family:Open-sans,Arial,sans-serif;font-size:small"> <strong>تغليف الهدايا</strong> </span>
                                                </td>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td bgcolor="#f8f8f8" colspan="4"
                                        style="border:1px solid #d6d4d4;color:#333;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse;margin:0 auto;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#333;padding:0"><span size="2"
                                                                                                     face="Open-sans, Arial, sans-serif"
                                                                                                     color="#555454"
                                                                                                     style="color:#555454;font-family:Open-sans,Arial,sans-serif;font-size:small"> {{$data['order']['gift']}} SAR </span>
                                                </td>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="conf_body">
                                    <td bgcolor="#f8f8f8" colspan="4"
                                        style="border:1px solid #d6d4d4;color:#333;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse;margin:0 auto;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#333;padding:0"><span size="2"
                                                                                                     face="Open-sans, Arial, sans-serif"
                                                                                                     color="#555454"
                                                                                                     style="color:#555454;font-family:Open-sans,Arial,sans-serif;font-size:small"> <strong>الشحن</strong> </span>
                                                </td>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td bgcolor="#f8f8f8" colspan="4"
                                        style="border:1px solid #d6d4d4;color:#333;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse;margin:0 auto;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#333;padding:0"><span size="2"
                                                                                                     face="Open-sans, Arial, sans-serif"
                                                                                                     color="#555454"
                                                                                                     style="color:#555454;font-family:Open-sans,Arial,sans-serif;font-size:small"> {{round($data['order']['total_shipping'])}} SAR </span>
                                                </td>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="conf_body">
                                    <td bgcolor="#f8f8f8" colspan="4"
                                        style="border:1px solid #d6d4d4;color:#333;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse;margin:0 auto;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#333;padding:0"><span size="2"
                                                                                                     face="Open-sans, Arial, sans-serif"
                                                                                                     color="#555454"
                                                                                                     style="color:#555454;font-family:Open-sans,Arial,sans-serif;font-size:small"> <strong>إجمالي الضريبة المدفوعة</strong> </span>
                                                </td>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td bgcolor="#f8f8f8" colspan="4"
                                        style="border:1px solid #d6d4d4;color:#333;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse;margin:0 auto;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#333;padding:0"><span size="2"
                                                                                                     face="Open-sans, Arial, sans-serif"
                                                                                                     color="#555454"
                                                                                                     style="color:#555454;font-family:Open-sans,Arial,sans-serif;font-size:small"> {{(round($data['order']['total_paid_tax_incl']) - round($data['order']['total_paid_tax_excl']))}} SAR </span>
                                                </td>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="conf_body">
                                    <td bgcolor="#f8f8f8" colspan="4"
                                        style="border:1px solid #d6d4d4;color:#333;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse;margin:0 auto;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#333;padding:0"><span size="2"
                                                                                                     face="Open-sans, Arial, sans-serif"
                                                                                                     color="#555454"
                                                                                                     style="color:#555454;font-family:Open-sans,Arial,sans-serif;font-size:small"> <strong>إجمالي المدفوعات</strong> </span>
                                                </td>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td bgcolor="#f8f8f8" colspan="4"
                                        style="border:1px solid #d6d4d4;color:#333;padding:7px 0">
                                        <table class="table" style="width:100%;border-collapse:collapse;margin:0 auto;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#333;padding:0"><span size="4"
                                                                                                     face="Open-sans, Arial, sans-serif"
                                                                                                     color="#555454"
                                                                                                     style="color:#555454;font-family:Open-sans,Arial,sans-serif;font-size:large;white-space: nowrap;"> {{round($data['order']['total_paid'])}} SAR </span>
                                                </td>
                                                <td width="10" style="color:#333;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="box" style="border:1px solid #d6d4d4;background-color:#f8f8f8;padding:7px 0">
                            <table class="table" style="width:100%">
                                <tbody>
                                <tr>
                                    <td width="10" style="padding:7px 0">&nbsp;</td>
                                    <td align="right" style="padding:7px 0; text-align: right;">
                                        <p style="border-bottom:1px solid #d6d4d4;margin:3px 0 7px;text-transform:uppercase;font-weight:500;font-size:18px;padding-bottom:10px;text-align: right;">
                                            الشحن</p>
                                        <span size="2" face="Open-sans, Arial, sans-serif" color="#555454"
                                              style="color:#555454;font-family:Open-sans,Arial,sans-serif;font-size:small">
                                            <span style="color:#777"> <span style="color:#333"></span> <strong>جهة الشحن :</strong> {{$data['carrier_detail']['name']}}<br><br>
                                                <span style="color:#333"></span> <strong>أسلوب الدفع :</strong> {{$data['order']['payment']}}
                                            </span>
                                        </span>
                                    </td>
                                    <td width="10" style="padding:7px 0">&nbsp;</td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="space_footer" style="padding:0!important">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="padding:7px 0">
                            <table class="table" style="width:100%">
                                <tbody>
                                <tr>
                                    <td class="box address" width="310"
                                        style="border:1px solid #d6d4d4;background-color:#f8f8f8;padding:7px 0">
                                        <table class="table" style="width:100%;margin:0 auto;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="padding:7px 0">&nbsp;</td>
                                                <td  align="right" style="padding:7px 0; text-align: right;">
                                                    <p style="border-bottom:1px solid #d6d4d4;margin:0px 0 7px;text-transform:uppercase;font-weight:500;font-size:18px;padding-bottom:10px;text-align: right;">
                                                        عنوان التوصيل</p>
                                                    <span size="2" face="Open-sans, Arial, sans-serif" color="#555454"
                                                          style="color:#555454;font-family:Open-sans,Arial,sans-serif;font-size:small; line-height: 12px;">
                                                        <span style="color:#777">
                                                            <span style="font-weight:bold">{{$data['delivery_address']['firstname']}}</span>
                                                            <span style="font-weight:bold">{{$data['delivery_address']['lastname']}}</span><br>
                                                            {{$data['delivery_address']['company']}}<br>
                                                            {{$data['delivery_address']['vat_number']}}<br>
                                                            {{$data['delivery_address']['address1']}}<br>
                                                            {{isset($data['delivery_address']['country']['details'][0]['name']) ? $data['delivery_address']['country']['details'][0]['name'] : ''}}<br>
                                                            {{$data['delivery_address']['postcode']}} {{$data['delivery_address']['city']}}<br>
                                                            {{$data['delivery_address']['phone']}}
                                                        </span>
                                                    </span>
                                                </td>
                                                <td width="10" style="padding:7px 0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="20" class="space_address" style="padding:7px 0">&nbsp;</td>
                                    <td class="box address" width="310"
                                        style="border:1px solid #d6d4d4;background-color:#f8f8f8;padding:7px 0">
                                        <table class="table" style="width:100%;margin:0 auto;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="padding:7px 0">&nbsp;</td>
                                                <td align="right" style="padding:7px 0; text-align: right;">
                                                    <p style="border-bottom:1px solid #d6d4d4;margin:0px 0 7px;text-transform:uppercase;font-weight:500;font-size:18px;padding-bottom:10px;text-align: right;">
                                                        عنوان الفواتير</p>
                                                    <span size="2" face="Open-sans, Arial, sans-serif" color="#555454"
                                                          style="color:#555454;font-family:Open-sans,Arial,sans-serif;font-size:small; line-height: 12px;">
                                                        <span style="color:#777">
                                                            <span style="font-weight:bold">{{$data['billing_address']['firstname']}}</span>
                                                            <span style="font-weight:bold">{{$data['billing_address']['lastname']}}</span><br>
                                                            {{$data['billing_address']['company']}}<br>
                                                            {{$data['billing_address']['vat_number']}}<br>
                                                            {{$data['billing_address']['address1']}}<br>
                                                            {{isset($data['delivery_address']['country']['details'][0]['name']) ? $data['delivery_address']['country']['details'][0]['name'] : ''}}<br>
                                                            {{$data['billing_address']['postcode']}} {{$data['billing_address']['city']}}<br>
                                                            {{$data['billing_address']['phone']}}
                                                        </span>
                                                    </span>
                                                </td>
                                                <td width="10" style="padding:7px 0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="space_footer" style="padding:0!important; text-align: right;"><strong>ملاحظة&nbsp;</strong>&nbsp;<span>تجهيز الطلب يأخذ من ثلاثة إلى خمسة أيام عمل</span> 
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="linkbelow" style="padding:7px 0; text-align: right;">
                            <span size="2" face="Open-sans, Arial, sans-serif" color="#555454"
                                  style="color:#555454;font-family:Open-sans,Arial,sans-serif;font-size:small">
                                <span> يمكنك متابعة طلبك وتحميل الفاتورة من
                                    <a href="{{Config::get('app.BASE_URL').app()->getLocale().'/order-history'}}" style="color:#337ff1"
                                       target="_blank" data-saferedirecturl="">"سجل الطلبيات"</a> من حساب العملاء عن طريق النقرعلى
                                    <a href="{{Config::get('app.BASE_URL').app()->getLocale().'/my-account'}}" style="color:#337ff1"
                                       target="_blank" data-saferedirecturl="">"حسابي"</a> على موقعنا
                                </span>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="linkbelow" style="padding:7px 0; text-align: right;">
                            <span size="2" face="Open-sans, Arial, sans-serif" color="#555454"
                                  style="color:#555454;font-family:Open-sans,Arial,sans-serif;font-size:small">
                                <span> إذا كان لديك حساب الزوار، يمكنك متابعة طلبيتك عن طريق
                                    <a href="{{Config::get('app.BASE_URL').app()->getLocale().'/guest-tracking?id_order='.$data['order']['reference']}}"
                                       style="color:#337ff1" target="_blank"
                                       data-saferedirecturl="">"ميزة تتبع الزوار"</a> في القسم الخاص على موقعنا
                                </span>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td class="space_footer" style="padding:0!important">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" class="footer" style="border-top:4px solid #333333;padding:7px 0; text-align: center;">
                            <span>
                                <a href="{{$data['shop_detail']['domain']}}" style="color:#337ff1" target="_blank" data-saferedirecturl="">{{$data['shop_detail']['name']}}</a>
                            </span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td class="space" style="width:20px;padding:7px 0">&nbsp;</td>
        </tr>
        </tbody>
    </table>
</div>
</div>
</body>
</html>
