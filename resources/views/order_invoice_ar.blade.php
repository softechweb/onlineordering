<!doctype html>
<html lang="ar">
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <style>
        * { font-family: DejaVu Sans, sans-serif; }
    </style>
</head>
<body>
<div style="background-color:#fff;width:100%;font-family:DejaVu Sans, sans-serif;color:#000;font-size:13px;line-height:18px;margin:auto">
    <table class="table table-mail" style="width:100%;margin-top:10px; direction: rtl;" direction="rtl">
        <tbody>
        <tr>
            <td  style="padding:7px 0">
                <table class="table" bgcolor="#ffffff" style="width:100%;direction: rtl;"  direction="rtl">
                    <tbody>
                    <tr>
                        <td align="right" class="logo" style="padding:7px 0; text-align: right;">
                            <a title="{{$shop_detail['name']}}" href="{{$shop_detail['domain']}}" style="color:#337ff1" target="_blank" data-saferedirecturl="">
                                <img src="{{$shop_detail['logo']}}" alt="{{$shop_detail['name']}}" data-image-whitelisted="" >
                            </a>
                        </td>
                        <td align="left" style="text-align: left;" class="invoice-code">
                            <p align="right" style="text-align: right; font-family: DejaVu Sans, sans-serif; margin: 0 0 5px 0;">
                                <strong>الفاتورة</strong>
                            </p>
                            <p align="right" style="text-align: right; font-family: DejaVu Sans, sans-serif; margin: 0 0 2px 0;">
                                <span class="date">{{date('m/d/Y', strtotime($order['date_add']))}}</span>
                            </p>
                            <p align="right" style="text-align: right; font-family: DejaVu Sans, sans-serif; margin: 0 0 2px 0;">
                                <span class="ref-id">#IN{{$order['id_order']}}</span>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="right" class="titleblock" style="padding:7px 0; text-align: right;">
                            <font size="3" face="DejaVu Sans, sans-serif" color="#000">
                           <span style="font-family: DejaVu Sans, sans-serif; text-align: right;">من: وصول الغذائية للإتصالات و تقنية المعلومات
                           <br>الرقم الضريبي: ٣٠٠٧٧٤٨٦٣٢٠٠٠٠٣
                           </span>
                            </font>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="12">
                            <font  face="Open-sans, arial, sans-serif" color="#000">
                                <h3 style="margin-bottom: 0; font-size: 15px; text-align: left; font-weight: bold; color: #000; padding-bottom: 20px; display: inline-block;">
                                     اتصل بنا :
                                </h3>
                                <span><strong style="margin: 0px; color: #000; text-align: left; font-family: Open-sans, arial, sans-serif; font-size: 15px; line-height: 18px;">هاتف : </strong> +966-920007913 </span>&nbsp;&nbsp;
                                <span><strong style="margin: 0px; color: #000; text-align: left; font-family: Open-sans, arial, sans-serif; font-size: 15px; line-height: 18px;">  البريد الإلكتروني : </strong> info@qavashop.com</span><br><br>
                            </font>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="space_footer" style="padding:0!important; height: 5px;"></td>
                    </tr>
                    <tr>
                        <td width="50%" class="box" align="right" style="padding:7px 0">
                            <font  face="DejaVu Sans, sans-serif" color="#000">
                               <span style=" font-size: 16px; font-weight: bold; color: #000; margin-bottom: 20px; display: inline-block; font-family: DejaVu Sans, sans-serif;">
                               عنوان التوصيل
                               </span><br>
                                <span style="color: #000; font-family: DejaVu Sans, sans-serif; font-size: 16px; line-height: 18px;">
                               {{$delivery_address['firstname']}} {{$delivery_address['lastname']}}<br>
                               {!! isset($delivery_address['company']) ? $delivery_address['company'].' <br>' : '' !!}
                                {!! isset($delivery_address['vat_number']) ? $delivery_address['vat_number'].' <br>' : '' !!}
                               {{$delivery_address['address1']}}<br>
                               {{isset($delivery_address['country']['details'][0]['name']) ? $delivery_address['country']['details'][0]['name'] : ''}}<br>
                               {{$delivery_address['postcode']}} {{$delivery_address['city']}}<br>
                               {{$delivery_address['phone']}}
                               </span>
                            </font>
                        </td>
                        <td width="50%" align="right" class="box" style="padding:7px 0">
                            <font  face="DejaVu Sans, sans-serif" color="#000">
                               <span style=" font-size: 16px; font-weight: bold; color: #000; margin-bottom: 20px; display: inline-block; font-family: DejaVu Sans, sans-serif;">
                               عنوان الفاتورة
                               </span><br>
                                <span style="color: #000; font-family: DejaVu Sans, sans-serif; font-size: 16px; line-height: 18px;">
                               {{$billing_address['firstname']}} {{$billing_address['lastname']}}<br>
                               {!! isset($billing_address['company']) ? $billing_address['company'].' <br>' : '' !!}
                                {!! isset($billing_address['vat_number']) ? $billing_address['vat_number'].' <br>' : '' !!}
                               {{$billing_address['address1']}}<br>
                               {{isset($billing_address['country']['details'][0]['name']) ? $billing_address['country']['details'][0]['name'] : ''}}<br>
                               {{$billing_address['postcode']}} {{$billing_address['city']}}<br>
                               {{$billing_address['phone']}}
                               </span>
                            </font>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding-top:5px;">
                            <table class="table table-recap" bgcolor="#ffffff" style="width:100%;border-collapse:collapse; border:1px solid #000;direction: rtl;">
                                <tbody>
                                <tr>
                                    <th bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family: DejaVu Sans, sans-serif;font-size:13px;padding:10px">رقم الفاتورة</th>
                                    <th bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family: DejaVu Sans, sans-serif;font-size:13px;padding:10px"> تاريخ الفاتورة </th>
                                    <th bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family: DejaVu Sans, sans-serif;font-size:13px;padding:10px" width="17%">  الرقم المرجعي للطلبية </th>
                                    <th bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family: DejaVu Sans, sans-serif;font-size:13px;padding:10px"> تاريخ طلب الشراء </th>
                                </tr>
                                <tr>
                                    <td align="center" style="background-color:#fff;color:#000;font-family:Arial;font-size:12px;padding:10px">
                                        #IN{{$order['id_order']}}
                                    </td>
                                    <td align="center" style="background-color:#fff;color:#000;font-family:Arial;font-size:12px;padding:10px">
                                        {{date('m/d/Y', strtotime($order['date_add']))}}
                                    </td>
                                    <td align="center" style="background-color:#fff;color:#000;font-family:Arial;font-size:12px;padding:10px">
                                        {{$order['reference']}}
                                    </td>
                                    <td align="center" style="background-color:#fff;color:#000;font-family:Arial;font-size:12px;padding:10px">
                                        {{date('m/d/Y', strtotime($order['date_add']))}}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="space_footer" style="padding:0!important;height: 5px;"></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <table class="table table-recap" bgcolor="#ffffff" style="width:100%;border-collapse:collapse; border: 1px solid #000;direction: rtl;">
                                <tbody>
                                <tr>
                                    <th width="100" bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family: DejaVu Sans, sans-serif;font-size:13px;padding:10px"> المرجع </th>
                                    <th bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family: DejaVu Sans, sans-serif;font-size:13px;padding:10px">منتج </th>
                                    <th width="120" bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family: DejaVu Sans, sans-serif;font-size:13px;padding:10px" width="120">معدل
                                        الضريبة </th>
                                    <th width="160" bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family: DejaVu Sans, sans-serif;font-size:13px;padding:10px"> (سعر الوحدة(غير شامل لضريبة القيمة المضافة </th>
                                    <th width="160" bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family: DejaVu Sans, sans-serif;font-size:13px;padding:10px"> الكمية </th>
                                    <th width="160" bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family: DejaVu Sans, sans-serif;font-size:13px;padding:10px"> (الإجمالي(غير شامل لضريبة القيمة المضافة </th>
                                </tr>

                                @foreach($order_detail as $product)
                                    <tr>
                                        <td align="center" style="background-color:#fff;color:#000;font-family: DejaVu Sans, sans-serif;font-size:11px;padding:10px">
                                            {{$product['product_reference']}}
                                        </td>
                                        <td align="center" style="background-color:#fff;color:#000;font-family: DejaVu Sans, sans-serif;font-size:12px;padding:10px">
                                            {{$product['product_name']}}
                                        </td>
                                        <td align="center" style="background-color:#fff;color:#000;font-family: DejaVu Sans, sans-serif;font-size:12px;padding:10px">
                                            {{--{{$product['reduction_percent']}} %--}}
                                            % 5.000
                                        </td>
                                        <td align="center" style="background-color:#fff;color:#000;font-family: DejaVu Sans, sans-serif;font-size:12px;padding:10px">
                                            {{round($product['unit_price_tax_excl'])}} ر.س
                                        </td>
                                        <td align="center" style="background-color:#fff;color:#000;font-family: DejaVu Sans, sans-serif;font-size:12px;padding:10px">
                                            {{$product['product_quantity']}}
                                        </td>
                                        <td align="center" style="background-color:#fff;color:#000;font-family: DejaVu Sans, sans-serif;font-size:12px;padding:10px">
                                            {{round($product['total_price_tax_excl'])}} ر.س
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="space_footer" style="padding:0!important;height: 5px;"></td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top; padding-left: 50px;">
                            <table class="table table-recap" bgcolor="#ffffff" style="width:100%;border-collapse:collapse;direction: rtl;">
                                <tbody>
                                <tr>
                                    <td colspan="2">
                                        <table class="table table-recap" bgcolor="#ffffff" style="width:100%;border-collapse:collapse; border: 1px solid #000;direction: rtl;">
                                            <tbody>
                                            <tr>
                                                <th bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family: DejaVu Sans, sans-serif;font-size:13px;padding:10px"> تفاصيل
                                                    ضريبة القيمة
                                                    المضافة </th>
                                                <th bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family: DejaVu Sans, sans-serif;font-size:13px;padding:10px"> معدل
                                                    الضريبة </th>
                                                <th bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family: DejaVu Sans, sans-serif;font-size:13px;padding:10px"> السعر
                                                    الابتدائى </th>
                                                <th bgcolor="#f0f0f0" style="background-color:#f0f0f0;color:#000;font-family:Arial;font-size:13px;padding:10px"> إجمالي
                                                    الضريبة </th>
                                            </tr>
                                            <tr>
                                                <td align="center" style="background-color:#fff;color:#000;font-family: DejaVu Sans, sans-serif;font-size:12px;padding:10px">
                                                    Products
                                                </td>
                                                <td align="center" style="background-color:#fff;color:#000;font-family: DejaVu Sans, sans-serif;font-size:12px;padding:10px">
                                                    % 5.000
                                                </td>
                                                <td align="center" style="background-color:#fff;color:#000;font-family: DejaVu Sans, sans-serif;font-size:12px;padding:10px">
                                                    {{ round($order['total_products']) }} ر.س
                                                </td>
                                                <td align="center" style="background-color:#fff;color:#000;font-family: DejaVu Sans, sans-serif;font-size:12px;padding:10px">
                                                    {{(round($order['total_paid_tax_incl']) - round($order['total_paid_tax_excl']))}} ر.س
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="space_footer" style="padding:0!important;height: 5px;"></td>
                                </tr>
                                <tr  class="conf_body">
                                    <td  bgcolor="#f0f0f0" style="font-family: DejaVu Sans, sans-serif;border-top: 1px solid #000; border-right: 1px solid #000; color:#000;padding:7px 10px">
                                        <strong style="font-family: DejaVu Sans, sans-serif;"> طريقة الدفع </strong>
                                    </td>
                                    <td bgcolor="#fff" align="right" style=" border-top: 1px solid #000; border-left: 1px solid #000;color:#000;padding:7px 10px">
                                        <span size="2" face="DejaVu Sans, sans-serif" color="#000" style="color:#000;white-space:nowrap;font-family: DejaVu Sans, sans-serif;font-size:small"> {{$order['payment']}} {{round($order['total_paid'])}} ر.س </span>
                                    </td>
                                </tr>
                                <tr class="conf_body">
                                    <td bgcolor="#f0f0f0" style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-right: 1px solid #000;color:#000;padding:7px 10px">
                                        <span size="2" face="DejaVu Sans, sans-serif" color="#000" style="color:#000;font-family: DejaVu Sans, sans-serif;font-size:small"> <strong  style="color:#000;font-family: DejaVu Sans, sans-serif;font-size:small"> جهة الشحن </strong> </span>
                                    </td>
                                    <td bgcolor="#fff"  style="border-bottom: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000;color:#000;padding:7px  10px">
                                        <span size="2" face="DejaVu Sans, sans-serif" color="#000" style="color:#000;font-family: DejaVu Sans, sans-serif;white-space:nowrap;font-size:small"> {{$carrier_detail['name']}} </span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>

                        <td style="vertical-align: top; padding-left: 0px;">
                            <table class="table" style="width:100%;border-collapse:collapse; border: 1px solid #000;direction: rtl;">
                                <tbody>
                                <tr  class="conf_body">
                                    <td  bgcolor="#f0f0f0" style="color:#000;padding:7px 0; border-right: 1px solid #000; border-top:1px solid #000;width:50%;">
                                        <table class="table" style="width:100%;border-collapse:collapse;direction: rtl;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0"><span size="2" face="DejaVu Sans, sans-serif" color="#000" style="color:#000;font-family: DejaVu Sans, sans-serif;font-size:small"> إجمالي المنتجات </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td bgcolor="#fff" align="right" style="color:#000;padding:7px 0; border-left: 1px solid #000; border-top:1px solid #000;width:50%;">
                                        <table class="table" style="width:100%;border-collapse:collapse;direction: rtl;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0"><span size="2" face="DejaVu Sans, sans-serif" color="#000" style="color:#000;font-family: DejaVu Sans, sans-serif;font-size:small"> {{ round($order['total_products']) }} ر.س </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr  class="conf_body">
                                    <td  bgcolor="#f0f0f0" style="color:#000;padding:7px 0; border-right: 1px solid #000;">
                                        <table class="table" style="width:100%;border-collapse:collapse;direction: rtl;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0"><span size="2" face="DejaVu Sans, sans-serif" color="#000" style="color:#000;font-family: DejaVu Sans, sans-serif;font-size:small"> رسوم الشحن </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td bgcolor="#fff" align="right" style="color:#000;padding:7px 0; border-left: 1px solid #000;">
                                        <table class="table" style="width:100%;border-collapse:collapse;direction: rtl;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0"><span size="2" face="DejaVu Sans, sans-serif" color="#000" style="color:#000;font-family: DejaVu Sans, sans-serif;font-size:small"> {{round($order['total_shipping'])}} ر.س </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr  class="conf_body">
                                    <td  bgcolor="#f0f0f0" style="color:#000;padding:7px 0; border-right: 1px solid #000;">
                                        <table class="table" style="width:100%;border-collapse:collapse;direction: rtl;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0"><span size="2" face="DejaVu Sans, sans-serif" color="#000" style="color:#000;font-family: DejaVu Sans, sans-serif;font-size:small"> <strong> (المجموع (غير شامل لضريبة القيمة المضافة
 </strong> </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td bgcolor="#fff" align="right" style="color:#000;padding:7px 0; border-left: 1px solid #000;">
                                        <table class="table" style="width:100%;border-collapse:collapse;direction: rtl;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0"><span size="2" face="DejaVu Sans, sans-serif" color="#000" style="color:#000;font-family: DejaVu Sans, sans-serif;font-size:small"><strong>{{round($order['total_paid_tax_excl'])}} ر.س</strong> </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr  class="conf_body">
                                    <td  bgcolor="#f0f0f0" style="color:#000;padding:7px 0; border-right: 1px solid #000;">
                                        <table class="table" style="width:100%;border-collapse:collapse;direction: rtl;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0"><span size="2" face="DejaVu Sans, sans-serif" color="#000" style="color:#000;font-family: DejaVu Sans, sans-serif;font-size:small"> <strong> إجمالي الضريبة </strong> </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td bgcolor="#fff" align="right" style="color:#000;padding:7px 0; border-left: 1px solid #000;">
                                        <table class="table" style="width:100%;border-collapse:collapse;direction: rtl;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0"><span size="2" face="DejaVu Sans, sans-serif" color="#000" style="color:#000;font-family: DejaVu Sans, sans-serif;font-size:small"><strong>{{(round($order['total_paid_tax_incl']) - round($order['total_paid_tax_excl']))}} ر.س</strong> </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr  class="conf_body">
                                    <td  bgcolor="#f0f0f0" style="color:#000;padding:7px 0; border-right: 1px solid #000; border-bottom: 1px solid #000;">
                                        <table class="table" style="width:100%;border-collapse:collapse;direction: rtl;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0"><span size="2" facfe="DejaVu Sans, sans-serif" color="#000" style="color:#000;font-family: DejaVu Sans, sans-serif;font-size:small"> <strong> الإجمالي </strong> </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td bgcolor="#fff" align="right" style="color:#000;padding:7px 0; border-left: 1px solid #000; border-bottom: 1px solid #000;">
                                        <table class="table" style="width:100%;border-collapse:collapse;direction: rtl;">
                                            <tbody>
                                            <tr>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                                <td align="right" style="color:#000;padding:0"><span size="2" face="DejaVu Sans, sans-serif" color="#000" style="color:#000;font-family: DejaVu Sans, sans-serif;font-size:small"><strong>{{round($order['total_paid'])}} ر.س</strong> </span></td>
                                                <td width="10" style="color:#000;padding:0">&nbsp;</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" align="center" class="footer" style=" text-align: center; padding:20px 0 8px">
                            <span style="font-size: 11px; color: #444444;">QavaShop - Saudi Arabia</span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>

