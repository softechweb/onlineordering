<?php

use App\Data\Models\AppRoleUser;
use \App\Data\Models\AppRole;
use App\Data\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = Carbon::now();

        $data =[
            //'id' => 1,
            'first_name' => 'Admin',
            'last_name' => 'User',
            'profile_image' => '',
            'phone_number' => 123456789,
            'email' => 'admin@online-order.com',
            'passwd' => bcrypt('123456789'),
            'status' => 'active',
            'activated_at' => $date,
            'date_add' => $date,
            'date_upd' => $date,
        ];

        //User::insertOnDuplicateKey($data);
        $user_id = DB::table('users')->insertGetId($data);

        $role_data = [
            'user_id' => $user_id,
            'role_id' => AppRole::ADMIN,
        ];

        AppRoleUser::create($role_data);

    }
}
