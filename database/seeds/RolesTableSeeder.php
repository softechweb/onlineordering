<?php

use Illuminate\Database\Seeder;
use App\Data\Models\AppRole;
use Carbon\Carbon;

class RolesTableSeeder extends Seeder
{

/**
* Run the database seeds.
*
* @return void
*/
public function run()
{
    $date = Carbon::now();        
    AppRole::insertOnDuplicateKey(array ( 
        array (
            'id' => AppRole::ADMIN,
            'title' => 'Admin',
            'scope' => json_encode([
                "user.view"
            ]),
            'created_at' => $date,
            'updated_at' => $date,
            'deleted_at' => NULL,
        )
    ));
}
}
