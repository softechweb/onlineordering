<?php

namespace App\Data\Models;

use App\Notifications\PasswordReset;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Yadakhov\InsertOnDuplicateKey;

class User extends Authenticatable
{
    protected $login_type;
    protected $webhook;

    use InsertOnDuplicateKey, Notifiable, HasApiTokens;

    const ACTIVE = 1;
    const IN_ACTIVE = 0;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'passwd',
    ];

    protected $with = [
        'roles'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function __construct(array $attributes = [])
    {
        $this->fillable = [
            'first_name', 'last_name', 'email', 'passwd', 'active', 'gender'
        ];

        $this->hidden = ['passwd'];

        parent::__construct($attributes);
    }

    public function getAuthPassword()
    {
        return $this->passwd;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function roles()
    {
        return $this->hasOne(AppRoleUser::class, 'user_id');
    }


    public function user_roles()
    {
        return $this->belongsToMany(AppRole::class,'app_role_users','user_id','role_id');
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordReset($token));
    }
}
