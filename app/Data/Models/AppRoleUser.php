<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Yadakhov\InsertOnDuplicateKey;

class AppRoleUser extends Model
{
    use InsertOnDuplicateKey;

    protected $table = 'app_role_users';

    protected $fillable = [
        'user_id', 'role_id'
    ];

}
