<?php

namespace App\Data\Models;

use Illuminate\Database\Eloquent\Model;
use Yadakhov\InsertOnDuplicateKey;

class AppRole extends Model
{
    use InsertOnDuplicateKey;

	const ADMIN = 1;

    protected $casts = [
        'scope' => 'array'
    ];

    public function user_roles()
    {
        return $this->belongsToMany(User::class,'app_role_users','role_id','user_id');
    }
}
