<?php
namespace App\Data\Contracts;

/**
 * @author syed waqas imam
 */
interface RepositoryContract {

    public function findById($id, $refresh = false);

    public function findByAll($pagination = false, $perPage = 10);

    public function create(array $data = []);

    public function update(array $data = [], $id);

    public function deleteById($id);

}
