<?php

namespace App\Data\Repositories;

use App\Data\Contracts\RepositoryContract;
use App\Data\Models\AppRole;
use App\Data\Models\User;
use App\Data\Models\UserDevice;
use App\Utils\Helper;

class UserRepository extends AbstractRepository implements RepositoryContract
{
    protected $model;

    protected $_cacheKey = 'user';
    protected $_cacheTotalKey = 'total-user';

    public function __construct(User $model) {
        $this->model = $model;
    }

    public function getUserById($id) {
        $data = User::find($id);
        return $data;
    }

    public function getUserByAttribute($attribute, $value)
    {
        $data = User::where($attribute, '=', $value)->with('roles')->first();
        if ($data != NULL) {
            return $data;
        }
        return false;
    }

    public function findById($id, $refresh = false, $details = false, $encode = true)
    {
        $data = parent::findById($id, $refresh, $details, $encode);

        if($data) {
            if (\request()->header('Login-From') == User::LOGIN_TYPE_ADMIN) {
                $role_id = AppRoleUser::where('user_id',$data->id)->value('role_id');
                $data->role = app('AppRoleRepository')->findById($role_id);
            }
        }

        return $data;
    }

    public function findByAll($pagination = false, $perPage = 10, $input = [])
    {
        if (isset($input['is_active'])) {
            $this->model = $this->model->where('status', $input['is_active']);
        }

        $ids = $this->model;

        if ($pagination) {
            $ids = $ids->paginate($perPage);
            $models = $ids->items();
        } else {
            $ids = $ids->get();
            $models = $ids;
        }

        $data = ['data'=>[]];
        if ($models) {
            foreach ($models as &$model) {
                $model = $this->findById($model->id);
                if ($model) {
                    $data['data'][] = $model;
                }
            }
        }

        if ($pagination) {
            // call method to paginate records
            $data = Helper::paginator($data, $ids);
        }
        return $data;
    }

    public function update(array $data = [], $id) {

        $model = $this->model->find($id);
        if ($model != NULL) {
            foreach ($data as $column => $value) {
                $model->{$column} = $value;
            }

            if ($model->save()) {
                return $this->findById($id, true);
            }
            return false;
        }
        return NULL;
    }

    public function getRoles() {
        $data = AppRole::all();
        return $data;
    }

    public function assignRole($userId, $roleId)
    {
        $user = User::find($userId);
        $user->user_roles()->sync([$roleId]);
    }

    public function getAllUserTokens()
    {
        $tokens = [];
        $result = UserDevice::all();
        if ($result) {
            $tokens = $result->pluck('device_token')->all();
            $tokens = array_unique($tokens);
        }
        return $tokens;
    }
}
