<?php

namespace App\Data\Repositories;

use App\Data\Contracts\RepositoryContract;
use App\Data\Models\AppRole;

class AppRoleRepository extends AbstractRepository implements RepositoryContract
{
    /**
     * These will hold the instance of AppRole Class.
     *
     * @var    object
     * @access protected
     **/
    protected $model;

    public function __construct(AppRole $model)
    {
        $this->model = $model;
    }

    protected $_cacheKey = 'app-role';
    protected $_cacheTotalKey = 'total-app-roles';

}
