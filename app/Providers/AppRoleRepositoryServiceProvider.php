<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Data\Models\AppRole;
use App\Data\Repositories\AppRoleRepository;

class AppRoleRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'AppRoleRepository', function () {
                return new AppRoleRepository(new AppRole);
            }
        );
    }
}
