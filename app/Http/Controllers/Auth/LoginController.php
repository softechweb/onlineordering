<?php

namespace App\Http\Controllers\Auth;

use App\Data\Models\AppRoleUser;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Data\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Data\Models\User;
use App\Data\Models\AppRole;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class LoginController extends Controller
{
    /*
   |--------------------------------------------------------------------------
   | Login Controller
   |--------------------------------------------------------------------------
   |
   | This controller handles authenticating users for the application and
   | redirecting them to your home screen. The controller uses a trait
   | to conveniently provide its functionality to your applications.
   |
   */
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $_userRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $repository)
    {
        $this->_userRepository = $repository;
        $this->middleware('guest')->except('logout');
    }

    /**
     * Handle a login request to the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            $code = Response::HTTP_NOT_ACCEPTABLE;
            $output = ['error' => ['code' => $code, 'message' => $validator->errors()->first()]];
            return response()->json($output, $code);
        }

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        if ($this->guard()->validate($this->credentials($request))) {

            $user = $this->guard()->getLastAttempted();

            if (\request()->header('Login-From') == User::LOGIN_TYPE_ADMIN) {
                if ($user->status == User::APP_STATUS_ACTIVE_TXT && $this->attemptLogin($request)) {
                    // Send the normal successful login response
                    return $this->sendLoginResponse($request);
                } else {
                    // Increment the failed login attempts and redirect back to the
                    // login form with an error message.
                    $this->incrementLoginAttempts($request);
                    return $this->sendBlockedLoginResponse($request);
                }
            } else {
                if ($user->active == User::ACTIVE && $this->attemptLogin($request)) {
                    // Send the normal successful login response
                    return $this->sendLoginResponse($request);
                } else {
                    // Increment the failed login attempts and redirect back to the
                    // login form with an error message.
                    $this->incrementLoginAttempts($request);
                    return $this->sendBlockedLoginResponse($request);
                }
            }
        }

        return $this->sendFailedLoginResponse($request);
    }

   /**
     * Send the response after the user was authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $this->clearLoginAttempts($request);

        $data = $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectPath());

        if (\request()->header('Login-From') == User::LOGIN_TYPE_ADMIN) {
            $user = app('UserRepository')->findById($data->id);

        } else {
            $user = app('UserRepository')->getUserById($data->id_customer);
        }

        if($request['id_cart']) {
            Cart::where('id_cart', $request['id_cart'])->update(['id_customer' => $user->id_customer]);
        }

        $output = ['access_token' => $data->access_token, 'data' => $user, 'message' => 'Success'];
        // HTTP_OK = 200;
        return response()->json($output, Response::HTTP_OK);
    }

    /**
     * The user has been authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @param mixed $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        if (!isset($user->roles->role_id)) {

            if (\request()->header('Login-From') == User::LOGIN_TYPE_ADMIN) {
                $user_id = $user['id'];

            } else {
                $user_id = $user['id_customer'];
            }

            $role = AppRoleUser::create([
                'user_id' => $user_id,
                'role_id' => AppRole::ADMIN
            ]);
            $user->roles = $role;
        }

        $scopes = (AppRole::find($user->roles->role_id)->scope) ? AppRole::find($user->roles->role_id)->scope : [];
        $user->access_token = $user->createToken('Token Name', $scopes)->accessToken;
        return $user;
    }

    /**
     * Log the user out of the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $user = $request->user();
        $user->token()->revoke();

        $output = ['data' => null, 'message' => 'Success'];
        // HTTP_OK = 200;
        return response()->json($output, Response::HTTP_OK);

        //return redirect('/');
    }

    /**
     * Get the failed login response instance.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendBlockedLoginResponse(Request $request)
    {
        $code = Response::HTTP_NOT_ACCEPTABLE;
        $output = ['error' => ['code' => $code, 'message' => trans('auth.blocked')]];
        return response()->json($output, $code);
    }

    protected function sendPendingLoginResponse(Request $request)
    {
        throw ValidationException::withMessages(
            [
                $this->username() => [trans('auth.pending')],
            ]
        );
    }

    /**
     * Get the failed login response instance.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $code = Response::HTTP_NOT_ACCEPTABLE;
        $output = ['error' => ['code' => $code, 'message' => trans('auth.failed')]];
        return response()->json($output, $code);
    }
}