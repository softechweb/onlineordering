<?php

namespace App\Http\Controllers\Auth;

use App\Data\Models\AppRole;
use App\Data\Models\AppRoleUser;
use App\Data\Models\Cart;
use App\Data\Models\CustomerGroup;
use App\Data\Models\User;
use App\Http\Controllers\Controller;
use Google_Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Socialite;
use App\Utils\SocialAuth;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make(
            $data, [
                'firstname' => 'required',
                'lastname' => 'required',
                'email' => 'required|email|unique:ps_customer,email',
                'password' => 'min:6|required',
            ]
        );
    }

    protected function socialValidator(array $data)
    {
        $validator =  Validator::make(
            $data, [
                'type' => 'required|in:facebook,google,normal'
            ]
        );
        $validator->sometimes('token', 'required|string', function ($input) {
            return in_array($input->type, ['facebook', 'google']);
        });
        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create(
            [
                'first_name' => $data['firstname'],
                'last_name' => $data['lastname'],
                'email' => $data['email'],
                'passwd' => Hash::make($data['password']),
                'active' => 1,
                'id_gender' => 1,
            ]
        );

        AppRoleUser::create([
            'user_id' => $user['id_customer'],
            'role_id' => AppRole::ADMIN
        ]);

        return $user;
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  mixed                    $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        $requestData = $request->all();

        $scopes = (AppRole::find($user->roles->role_id)->scope) ? AppRole::find($user->roles->role_id)->scope:[];
        $user->access_token = $token = $user->createToken('Token Name', $scopes)->accessToken;

        $output = ['access_token'=>$user->access_token, 'data' => app('UserRepository')->getUserById($user->id_customer,false), 'message'=> "Registration Successfully"];
        // HTTP_OK = 200;
        return response()->json($output, Response::HTTP_OK);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $requestData = $request->all();

        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $code = Response::HTTP_NOT_ACCEPTABLE;
            $output = ['error' => ['code' => $code, 'message' => $validator->errors()->first()]];
            return response()->json($output, $code);
        }

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }
}
