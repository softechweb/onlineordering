<?php

namespace App\Http\Controllers\Api;

use App\Data\Repositories\UserRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\CreateSubadminRequest;
use App\Http\Requests\Api\UpdateSubadminRequest;
use App\Http\Requests\Api\UpdateUserProfileRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    public $_repository;
    const   PER_PAGE = 10;
    protected $model;

    public function __construct(UserRepository $repository)
    {
        $this->_repository = $repository;
    }

    public function getAuthUser(Request $request)
    {
        $user_id = $request->user()->id_customer;
        $data = $this->_repository->getUserById($user_id);
        $output = ['data' => $data, 'code' => Response::HTTP_OK, 'message' => 'Success', ];

        return response()->json($output);

    }

    //Update User Profile
    public function updateUserProfile(UpdateUserProfileRequest $request, $id)
    {
        $requestData = $request->only(['firstname','lastname','old_password','passwd']);

        $data = $this->_repository->getUserById($id);
        $message = __("messages.update");

        if(isset($requestData['passwd']) && isset($requestData['old_password'])) {
            if (Hash::check($requestData['old_password'], $data->passwd)) {
                $requestData['passwd'] = Hash::make($requestData['passwd']);
            } else {
                return response()->json(['error' => ['code' => Response::HTTP_NOT_ACCEPTABLE, 'message' => __("messages.old_password_incorrect")]], Response::HTTP_NOT_ACCEPTABLE);
            }
        }

        unset($requestData['old_password']);
        $this->_repository->update($requestData, $id);
        $data = $this->_repository->getUserById($id);

        $output = ['data' => $data, 'message' => $message];
        return response()->json($output, Response::HTTP_OK);
    }

    //Get all Subadmins
    public function index(Request $request)
    {
        $requestData = $request->all();

        $per_page = self::PER_PAGE ? self::PER_PAGE : config('app.per_page');
        $pagination = !empty($requestData['pagination']) ? $requestData['pagination'] : false;
        $data = $this->_repository->findByAll($pagination, $per_page, $requestData);

        $output = [
            'data' => $data['data'],
            'pagination' => !empty($data['pagination']) ? $data['pagination'] : false,
            'message' => __('messages.success'),
        ];

        return response()->json($output, Response::HTTP_OK);
    }

    //Add Subadmin
    public function store(CreateSubadminRequest $request)
    {
        $requestData = $request->only(['first_name','last_name','email','passwd','phone_number','status']);

        $requestData['passwd'] = Hash::make($requestData['passwd']);
        $data = $this->_repository->create($requestData);

        $role = $request->get('role');
        if($role) {
            $this->_repository->assignRole($data->id,$role);
        }

        $output = ['data' => $data, 'message' => __("messages.store")];
        return response()->json($output, Response::HTTP_OK);
    }

    //Update Subadmin
    public function update(UpdateSubadminRequest $request, $id)
    {
        $requestData = $request->only(['first_name','last_name','email','passwd','phone_number','status']);

        if($requestData['passwd']) {
            $requestData['passwd'] = Hash::make($requestData['passwd']);
        }

        $data = $this->_repository->update($requestData, $id);

        $role = $request->get('role');
        if($role) {
            $this->_repository->assignRole($data->id,$role);
        }

        $output = ['data' => $data, 'message' => __("messages.update")];
        return response()->json($output, Response::HTTP_OK);
    }

    //Delete Subadmin
    public function destroy($id)
    {
        $data = $this->_repository->deleteById($id);

        $output = ['data' => $data, 'message' => __("messages.destroy")];
        return response()->json($output, Response::HTTP_OK);
    }

    //Get Subadmin single record
    public function show(Request $request,  $id)
    {
        $data = $this->_repository->findById($id);

        $output = ['data' => $data, 'message' => __("messages.success")];
        return response()->json($output, Response::HTTP_OK);
    }

    public function getRoles()
    {
        $data = $this->_repository->getRoles();
        $output = ['data' => $data, 'code' => Response::HTTP_OK, 'message' => 'Success', ];

        return response()->json($output);
    }
}
