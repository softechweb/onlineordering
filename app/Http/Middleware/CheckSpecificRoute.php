<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Response;

class CheckSpecificRoute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = 'AAGxgVnN1MF7OHO7pfBCs2tG0kUK3qrnNfDxgPvR';
        $authHeader = $request->header('Authorization');
        $authQS = $request->get('token');
        $auth = $authHeader;
        if (!$authHeader) {
            $auth = $authQS;
        }
        if ($auth == $token) {
            return $next($request);
        } else {
            $code = Response::HTTP_UNAUTHORIZED;
            $output = [
                'message' => 'Unauthenticated User',
            ];
            return response()->json($output, $code);
        }

    }
}
