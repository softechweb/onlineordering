<?php

namespace App\Http\Requests\Api;

/**
 * Class UpdateSubadminRequest
 * @package App\Http\Requests\Api
 */
class UpdateSubadminRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email',
            'passwd' => 'required',
            'phone_number' => 'required',
            //'profile_image' => 'required',
            'status' => 'required|in:pending,active,banned,deactivated',
            'role' => 'required',
        ];

        return $rules;
    }

}
