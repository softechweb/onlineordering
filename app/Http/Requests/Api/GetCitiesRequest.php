<?php

namespace App\Http\Requests\Api;

use Illuminate\Support\Facades\Request;

/**
 * Class GetCitiesRequest
 * @package App\Http\Requests\Api
 */
class GetCitiesRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'pagination' =>  'nullable|boolean',
            'is_active'  =>  'boolean',
            'country_id' =>  'required|exists:ps_city,id_country'
        ];

        return $rules;
    }
}
