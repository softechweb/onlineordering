<?php

namespace App\Http\Requests\Api;

use App\Data\Models\User;
use Illuminate\Validation\Rule;

/**
 * Class AddCartRequest
 * @package App\Http\Requests\Api
 */
class SuccessPaymentCallbackRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'order_id' => 'required|exists:ps_orders,id_order',
            'cart_id' => 'required|exists:ps_cart,id_cart',
            'order_reference' => 'required|exists:ps_orders,reference',
            'cko-session-id' => 'required',
            'token' => 'required',Rule::in([User::AUTH_TOKEN]),
            'device_type' => 'required'
        ];

        return $rules;
    }
}
