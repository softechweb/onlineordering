<?php

namespace App\Http\Requests\Api;

/**
 * Class CreateAddressRequest
 * @package App\Http\Requests\Api
 */
class CreateAddressRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id_country' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
            'address1' => 'required',
          //  'postcode' => 'required',
            'city' => 'required',
            'phone' => 'required'
        ];

        return $rules;
    }
}
