<?php

namespace App\Http\Requests\Api;

/**
 * Class CheckCartRequest
 * @package App\Http\Requests\Api
 */
class CheckCartRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id' =>  'required|exists:ps_cart,id_cart',
        ];

        return $rules;
    }

    protected function validationData()
    {
        return array_merge($this->request->all(), [
            'id' => $this->route('id'),
        ]);
    }
}
