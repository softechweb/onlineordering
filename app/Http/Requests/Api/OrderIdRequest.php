<?php

namespace App\Http\Requests\Api;

/**
 * Class OrderIdRequest
 * @package App\Http\Requests\Api
 */
class OrderIdRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id_order' => 'required|exists:ps_orders,id_order',
        ];

        return $rules;
    }
}
