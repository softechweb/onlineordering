<?php

namespace App\Http\Requests\Api;

use App\Data\Models\OrderDeviceType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\Rule;

/**
 * Class CreateOrderRequest
 * @package App\Http\Requests\Api
 */
class CreateOrderRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $rules = [
            'id_cart' => 'required|exists:ps_cart,id_cart|unique:ps_orders,id_cart',
            'module' => 'required|in:ps_checkpayment,codfee,checkoutapipayment',
            'device_type' => 'required|'.Rule::in(OrderDeviceType::$DEVICE_TYPES),
        ];

        if ($request->get('module') == Config::get('constants.payment_module.checkoutapipayment')) {
            $rules['card_number'] = 'required';
            $rules['card_month'] = 'required';
            $rules['card_year'] = 'required';
            $rules['card_cvv'] = 'required';
            $rules['currency'] = 'required';
        }

        return $rules;
    }
}
