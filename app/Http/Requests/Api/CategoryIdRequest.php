<?php

namespace App\Http\Requests\Api;

/**
 * Class CategoryIdRequest
 * @package App\Http\Requests\Api
 */
class CategoryIdRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id_category' => 'required|exists:ps_category,id_category',
        ];

        return $rules;
    }
}
