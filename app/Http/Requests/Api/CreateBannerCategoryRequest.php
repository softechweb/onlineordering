<?php

namespace App\Http\Requests\Api;

/**
 * Class CreateBannerCategoryRequest
 * @package App\Http\Requests\Api
 */
class CreateBannerCategoryRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'details' =>  'required',
            'active'  =>  'required|boolean'
        ];

        return $rules;
    }
}
