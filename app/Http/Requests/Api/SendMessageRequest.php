<?php

namespace App\Http\Requests\Api;

/**
 * Class SendMessageRequest
 * @package App\Http\Requests\Api
 */
class SendMessageRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id_customer_thread' => 'required|exists:ps_customer_thread,id_customer_thread',
            'message' => 'required',
        ];

        return $rules;
    }
}
