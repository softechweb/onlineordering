<?php

namespace App\Http\Requests\Api;

/**
 * Class OrderIdRequest
 * @package App\Http\Requests\Api
 */
class ProductIdRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id_product' => 'required|exists:ps_product,id_product',
        ];

        return $rules;
    }
}
