<?php

namespace App\Http\Requests\Api;

/**
 * Class CreateWishlistRequest
 * @package App\Http\Requests\Api
 */
class CreateWishlistRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id_product' => 'required'
        ];

        return $rules;
    }
}
