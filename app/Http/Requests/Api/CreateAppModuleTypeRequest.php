<?php

namespace App\Http\Requests\Api;

/**
 * Class CreateAppModuleTypeRequest
 * @package App\Http\Requests\Api
 */
class CreateAppModuleTypeRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'module' => 'required|in:banner,product,manufacturer,category',
            'slug' => 'required|string',
            'title' => 'required|string',
            'status' => 'required|integer',
        ];

        return $rules;
    }
}
