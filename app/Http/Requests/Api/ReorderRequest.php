<?php

namespace App\Http\Requests\Api;

/**
 * Class ReorderRequest
 * @package App\Http\Requests\Api
 */
class ReorderRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id_cart' =>  'required|exists:ps_cart,id_cart',
        ];

        return $rules;
    }

}
