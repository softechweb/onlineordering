<?php

namespace App\Http\Requests\Api;

/**
 * Class UpdateUserProfileRequest
 * @package App\Http\Requests\Api
 */
class UpdateUserProfileRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'firstname' => 'required|string',
            'lastname' => 'required|string',
            'passwd' => 'min:6',
            //'phone_number' => 'required',
            'old_password' => 'required_with:passwd'
        ];

        return $rules;
    }

}
