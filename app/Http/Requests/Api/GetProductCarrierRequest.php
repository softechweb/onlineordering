<?php

namespace App\Http\Requests\Api;

/**
 * Class GetProductCarrierRequest
 * @package App\Http\Requests\Api
 */
class GetProductCarrierRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
             'id_product' => 'required|array',
             'id_product.*' => 'exists:ps_product,id_product', // check each item in the array
             'city' => 'required',
        ];

        return $rules;
    }
}
