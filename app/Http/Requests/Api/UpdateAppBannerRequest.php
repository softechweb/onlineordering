<?php

namespace App\Http\Requests\Api;

/**
 * Class UpdateAppBannerRequest
 * @package App\Http\Requests\Api
 */
class UpdateAppBannerRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id' =>  'required|exists:mongodb.app_landing_banners,_id',
            'type' =>  'required|in:slider_banners,single_banner,category_banners,featured_banners,video_banner,detail_page_banner',
            //'detail' =>  'required'
        ];

        return $rules;
    }

    protected function validationData()
    {
        return array_merge($this->request->all(), [
            'id' => $this->route('banner'),
        ]);
    }
}
