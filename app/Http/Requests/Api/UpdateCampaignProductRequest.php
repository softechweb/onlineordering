<?php

namespace App\Http\Requests\Api;

/**
 * Class UpdateProductRequest
 * @package App\Http\Requests\Api
 */
class UpdateCampaignProductRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id' =>  'required|exists:mongodb.campaign_products,_id',
            'is_active' =>  'boolean'
        ];

        return $rules;
    }

    protected function validationData()
    {
        return array_merge($this->request->all(), [
            'id' => $this->route('id'),
        ]);
    }
}
