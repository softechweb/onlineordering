<?php

namespace App\Http\Requests\Api;

/**
 * Class UpdateCartRequest
 * @package App\Http\Requests\Api
 */
class UpdateCartRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id_product' => 'required',
            'quantity' => 'required',
            'id_product_attribute' => 'required',
            'id' =>  'required|exists:ps_cart,id_cart',
        ];

        return $rules;
    }

    protected function validationData()
    {
        return array_merge($this->request->all(), [
            'id' => $this->route('id'),
        ]);
    }
}
