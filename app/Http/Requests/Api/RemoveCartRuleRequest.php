<?php

namespace App\Http\Requests\Api;

/**
 * Class RemoveCartRuleRequest
 * @package App\Http\Requests\Api
 */
class RemoveCartRuleRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'id_cart'    =>  'required',
            'id_cart_rule' =>  'required'
        ];

        return $rules;
    }
}
