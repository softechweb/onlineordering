<?php

namespace App\Http\Requests\Api;

/**
 * Class CreateCampaignRequest
 * @package App\Http\Requests\Api
 */
class CreateCampaignRequest extends BaseAPIRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title_en' => 'required|string',
            'title_ar' => 'required|string',
            'active' => 'required',
        ];

        return $rules;
    }
}
